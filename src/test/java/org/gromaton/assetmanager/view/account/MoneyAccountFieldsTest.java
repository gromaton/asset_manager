package org.gromaton.assetmanager.view.account;


class MoneyAccountFieldsTest {
    /*private static MoneyAccountFields fields;
    private static MoneyAccount mockedMoneyAccount;


    @BeforeAll
    static void beforeAll() {
       fields = new MoneyAccountFields();
    }

    @BeforeEach
    void beforeEach() {
        MoneyAccount moneyAccount = mock(MoneyAccount.class);
        when(moneyAccount.getBalance()).thenReturn(new BigDecimal("100.00"));
        when(moneyAccount.getName()).thenReturn("Some name");
        when(moneyAccount.getSymbol()).thenReturn("EUR");
        this.mockedMoneyAccount = moneyAccount;
    }

    @Test
    void setEntity_testProperInputFieldsPopulating() {
        fields.setEntity(mockedMoneyAccount);
        Assertions.assertAll("Setting all input fields from moneyAccount mock",
                () -> assertEquals(mockedMoneyAccount.getBalance(), fields.balance.getValue()),
                () -> assertEquals(mockedMoneyAccount.getName(), fields.name.getValue()),
                () -> assertEquals(mockedMoneyAccount.getSymbol(), fields.symbol.getValue())
        );
    }

    @Test
    void getEntity_testProperFieldsReading_WhenAccountIsWithoutID() {
        fields.reset();
        fields.balance.setValue(new BigDecimal("50.1234"));
        fields.name.setValue("new");
        fields.symbol.setValue("USD");
        MoneyAccount moneyAccountFromFields = fields.getEntity();
        Assertions.assertAll("Getting CryptoAccount instance from input fields",
                () -> assertEquals(new BigDecimal("50.12"), moneyAccountFromFields.getBalance()),
                () -> assertEquals("new", moneyAccountFromFields.getName()),
                () -> assertEquals("USD", moneyAccountFromFields.getSymbol()),
                () -> assertEquals(0, moneyAccountFromFields.getId())
        );
    }

    @Test
    void getEntity_testProperFieldsReading_WhenAccountIsWithID() {
        when(mockedMoneyAccount.getId()).thenReturn(1L);
        fields.setEntity(mockedMoneyAccount);
        MoneyAccount moneyAccountFromFields = fields.getEntity();
        Assertions.assertAll("Getting MoneyAccount instance from input fields",
                () -> assertEquals(fields.balance.getValue(), moneyAccountFromFields.getBalance()),
                () -> assertEquals(fields.name.getValue(), moneyAccountFromFields.getName()),
                () -> assertEquals(fields.symbol.getValue(), moneyAccountFromFields.getSymbol()),
                () -> assertEquals(1L, moneyAccountFromFields.getId())
        );
    }

    @Test
    void reset_testInputFieldsAreSetToDefault() {
        fields.setEntity(mockedMoneyAccount);
        fields.reset();
        MoneyAccountFields defaultFields = new MoneyAccountFields();
        Assertions.assertAll("All input fields are cleared",
                () -> assertEquals(defaultFields.balance.getValue(), fields.balance.getValue()),
                () -> assertEquals(defaultFields.name.getValue(), fields.name.getValue()),
                () -> assertEquals(defaultFields.symbol.getValue(), fields.symbol.getValue())
        );
    }

    @Test
    void reset_testReturnedEntityIdIsZeroAfterReset() {
        when(mockedMoneyAccount.getId()).thenReturn(1L);
        fields.setEntity(mockedMoneyAccount);
        Assertions.assertEquals(1L, fields.getEntity().getId());
        fields.reset();
        Assertions.assertEquals(0, fields.getEntity().getId());
    }*/
}