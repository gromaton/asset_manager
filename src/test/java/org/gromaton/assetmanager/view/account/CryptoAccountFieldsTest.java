package org.gromaton.assetmanager.view.account;


class CryptoAccountFieldsTest {
    /*private static CryptoAccountFields fields;
    private static CryptoAccount mockedCryptoAccount;

    @BeforeAll
    static void beforeAll() {
        fields = new CryptoAccountFields();
    }

    @BeforeEach
    void beforeEach() {
        CryptoAccount cryptoAccount = mock(CryptoAccount.class);
        when(cryptoAccount.getBalance()).thenReturn(new BigDecimal("1.00001"));
        when(cryptoAccount.getScale()).thenReturn(5);
        when(cryptoAccount.getName()).thenReturn("Some name");
        when(cryptoAccount.getSymbol()).thenReturn("ETH");
        this.mockedCryptoAccount = cryptoAccount;
    }

    @Test
    void setEntity_testProperInputFieldsPopulating() {
        fields.setEntity(mockedCryptoAccount);
        Assertions.assertAll("Setting all input fields from cryptoAccount mock",
                () -> assertEquals(mockedCryptoAccount.getBalance(), fields.balance.getValue()),
                () -> assertEquals(mockedCryptoAccount.getScale(), fields.scale.getValue()),
                () -> assertEquals(mockedCryptoAccount.getName(), fields.name.getValue()),
                () -> assertEquals(mockedCryptoAccount.getSymbol(), fields.symbol.getValue())
        );
    }

    @Test
    void getEntity_testProperFieldsReading_WhenAccountIsWithoutID() {
        fields.reset();
        fields.balance.setValue(new BigDecimal("1.1234"));
        fields.scale.setValue(10);
        fields.name.setValue("bitcoin");
        fields.symbol.setValue("BTC");
        CryptoAccount cryptoAccountFromFields = fields.getEntity();
        Assertions.assertAll("Getting CryptoAccount instance from input fields",
                () -> assertEquals(new BigDecimal("1.1234000000"), cryptoAccountFromFields.getBalance()),
                () -> assertEquals(10, cryptoAccountFromFields.getScale()),
                () -> assertEquals("bitcoin", cryptoAccountFromFields.getName()),
                () -> assertEquals("BTC", cryptoAccountFromFields.getSymbol()),
                () -> assertEquals(0, cryptoAccountFromFields.getId())
        );
    }

    @Test
    void getEntity_testProperFieldsReading_WhenAccountIsWithID() {
        when(mockedCryptoAccount.getId()).thenReturn(1L);
        fields.setEntity(mockedCryptoAccount);
        CryptoAccount cryptoAccountFromFields = fields.getEntity();
        Assertions.assertAll("Getting CryptoAccount instance from input fields",
                () -> assertEquals(fields.balance.getValue(), cryptoAccountFromFields.getBalance()),
                () -> assertEquals(fields.scale.getValue(), cryptoAccountFromFields.getScale()),
                () -> assertEquals(fields.name.getValue(), cryptoAccountFromFields.getName()),
                () -> assertEquals(fields.symbol.getValue(), cryptoAccountFromFields.getSymbol()),
                () -> assertEquals(1L, cryptoAccountFromFields.getId())
        );
    }

    @Test
    void reset_testInputFieldsAreSetToDefault() {
        fields.setEntity(mockedCryptoAccount);
        fields.reset();
        CryptoAccountFields defaultFields = new CryptoAccountFields();
        Assertions.assertAll("All input fields are cleared",
                () -> assertEquals(defaultFields.balance.getValue(), fields.balance.getValue()),
                () -> assertEquals(defaultFields.scale.getValue(), fields.scale.getValue()),
                () -> assertEquals(defaultFields.name.getValue(), fields.name.getValue()),
                () -> assertEquals(defaultFields.symbol.getValue(), fields.symbol.getValue())
        );
    }

    @Test
    void reset_testIfReturnedEntityIdIsZeroAfterReset() {
        when(mockedCryptoAccount.getId()).thenReturn(1L);
        fields.setEntity(mockedCryptoAccount);
        Assertions.assertEquals(1L, fields.getEntity().getId());
        fields.reset();
        Assertions.assertEquals(0, fields.getEntity().getId());
    }*/
}