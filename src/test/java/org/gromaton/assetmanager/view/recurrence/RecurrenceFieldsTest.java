package org.gromaton.assetmanager.view.recurrence;

import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecurrenceFieldsTest {
    @Mock
    RecurrenceFieldsFactory fieldFactory;
    @Mock
    RecurrenceCondition recCond1;
    RecurrenceCondition recCond2 = new StubRecurrenceCondition();

    GenericEntityFields<RecurrenceCondition> stubFields1;
    GenericEntityFields<RecurrenceCondition> stubFields2;
    private RecurrenceFields testObject;

    @BeforeEach
    void beforeEach() {
        stubFields1 = new StubEntityFields();
        stubFields2 = new StubEntityFields();
        when(fieldFactory.getAllClasses()).thenReturn(Arrays.asList(recCond1.getClass(), recCond2.getClass()));
        when(fieldFactory.getTitle(recCond1.getClass())).thenReturn("class1");
        when(fieldFactory.getTitle(recCond2.getClass())).thenReturn("class2");
        lenient().when(fieldFactory.create(recCond1.getClass())).thenReturn(stubFields1);
        lenient().when(fieldFactory.create(recCond2.getClass())).thenReturn(stubFields2);
        testObject = new RecurrenceFields(fieldFactory);

    }

    @Test
    void testCreation() {
        assertEquals(2, fieldFactory.getAllClasses().size());
        assertNotEquals(recCond1, recCond2);
        assertNotEquals(recCond1.getClass(), recCond2.getClass());
        assertNotNull(testObject);
    }

    @Test
    void testIfNewFieldsAreSet_AfterTypeSelectBoxValueChanges() {
        stubFields1.setEntity(recCond1);
        stubFields2.setEntity(recCond2);
        testObject.typeSelect.setValue(recCond1.getClass());
        assertEquals(recCond1, testObject.getEntity());
        testObject.typeSelect.setValue(recCond2.getClass());
        assertEquals(recCond2, testObject.getEntity());
    }

    @Test
    void testIfProperEntityTypeIsReadFromFields_AfterThisTypeWasSetIntoRecurrenceFields() {
        testObject.setEntity(recCond1);
        assertEquals(recCond1, testObject.getEntity());
        testObject.setEntity(recCond2);
        assertEquals(recCond2, testObject.getEntity());
        testObject.setEntity(recCond1);
        assertEquals(recCond1, testObject.getEntity());
    }

    private static class StubEntityFields extends GenericEntityFields<RecurrenceCondition> {
        RecurrenceCondition entity;

        @Override
        public RecurrenceCondition getEntity() {
            return entity;
        }

        @Override
        public void setEntity(RecurrenceCondition entity) {
            this.entity = entity;
        }

        @Override
        public void setDefaultValuesSource(RecurrenceCondition source) {

        }

        @Override
        public Class<RecurrenceCondition> getEntityClass() {
            return null;
        }

        @Override
        public void reset() {

        }
    }

    private static class StubRecurrenceCondition implements RecurrenceCondition {

        @Override
        public LocalDateTime nextEventAfter(LocalDateTime timeStamp) {
            return null;
        }

        @Override
        public RecurrenceCondition copy() {
            return null;
        }

        @Override
        public String title() {
            return "Stub";
        }
    }

}