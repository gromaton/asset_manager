package org.gromaton.assetmanager.view.recurrence;

import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
/*
class RecurrenceFieldsFactoryImplTest {
    @Autowired
    List<GenericEntityFields<? extends RecurrenceCondition>> availableFields;
    RecurrenceFieldsFactoryImpl fieldsFactory;
    List<GenericEntityFields<? extends RecurrenceCondition>> fieldsList = new ArrayList<>();

    @PostConstruct
    public void init() {
        fieldsFactory = new RecurrenceFieldsFactoryImpl(availableFields);
    }

    @BeforeEach
    void beforeEach() {
        for (GenericEntityFields<? extends RecurrenceCondition> fields : availableFields) {
            fieldsList.add( fieldsFactory.create(fields.getEntityClass()));
        }
    }
    @Test
    void testCreate() {
        for (GenericEntityFields<? extends RecurrenceCondition> testFields : fieldsList) {
            assertNotNull(testFields);
        }
    }

    @Test
    void testGetAllTitles() {

    }
}*/

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RecurrenceFieldsFactoryImplTest {
    @Autowired
    RecurrenceFieldsFactoryImpl testFactory;
    @Autowired
    List<GenericEntityFields<? extends RecurrenceCondition>> availableFields;
    List<GenericEntityFields<? extends RecurrenceCondition>> fieldsList = new ArrayList<>();

    @PostConstruct
    public void init() {
        testFactory = new RecurrenceFieldsFactoryImpl(availableFields);
    }



    @ParameterizedTest
    @MethodSource("recurrenceClassesProvider")
    void testCreate(Class<? extends RecurrenceCondition> clazz) {
        GenericEntityFields<RecurrenceCondition> fields = testFactory.create(clazz);
        assertNotNull(fields);
        assertEquals(clazz, fields.getEntityClass());
    }

    @Test
    void testGetAllTitles() {

    }

    private Stream<Class<? extends RecurrenceCondition>> recurrenceClassesProvider() {
        return availableFields.stream().map(e -> e.getEntityClass());
    }
}
