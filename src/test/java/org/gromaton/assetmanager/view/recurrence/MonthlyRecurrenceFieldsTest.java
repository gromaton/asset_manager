package org.gromaton.assetmanager.view.recurrence;

import org.gromaton.assetmanager.domain.recurrence.Monthly;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MonthlyRecurrenceFieldsTest {

    private MonthlyRecurrenceFields fields = new MonthlyRecurrenceFields();
    private Monthly monthly;

    @BeforeEach
    void beforeEach() {
        monthly = mock(Monthly.class);
    }

    @Test
    void getEntity() {
        when(monthly.getDayOfMonth()).thenReturn(5);
        fields.setEntity(monthly);
        assertEquals(5, fields.getEntity().getDayOfMonth());
        fields.daySelect.setValue(4);
        assertEquals(4, fields.getEntity().getDayOfMonth());
    }

    @Test
    void setEntity() {
        when(monthly.getDayOfMonth()).thenReturn(5);
        fields.setEntity(monthly);
        assertEquals(5, fields.daySelect.getValue());
        when(monthly.getDayOfMonth()).thenReturn(31);
        fields.setEntity(monthly);
        assertEquals(31, fields.daySelect.getValue());

    }

    @Test
    void setDefaultValuesSource() {
        when(monthly.getDayOfMonth()).thenReturn(31);
        fields.setDefaultValuesSource(monthly);
        fields.reset();
        assertEquals(31, fields.daySelect.getValue());
    }

    @Test
    void getEntityClass() {
        assertEquals(Monthly.class, fields.getEntityClass());
    }

    @Test
    void resetDaySelectValueIsSetToDefault() {
        when(monthly.getDayOfMonth()).thenReturn(31);
        fields.setDefaultValuesSource(monthly);
        fields.reset();
        assertEquals(31, fields.daySelect.getValue());
        when(monthly.getDayOfMonth()).thenReturn(7);
        fields.setDefaultValuesSource(monthly);
        fields.reset();
        assertEquals(7, fields.daySelect.getValue());
    }

    @Test
    void reset_FormBeanAlsoResets() {
        when(monthly.getDayOfMonth()).thenReturn(31);
        when(monthly.getId()).thenReturn(32L);
        fields.setEntity(monthly);
        fields.reset();
        assertNotEquals(32L, fields.getEntity().getId());
        assertNotEquals(31, fields.getEntity().getDayOfMonth());
    }
}