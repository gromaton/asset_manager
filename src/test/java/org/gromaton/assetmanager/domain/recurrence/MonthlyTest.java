package org.gromaton.assetmanager.domain.recurrence;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class MonthlyTest {
    LocalTime time;
    Monthly monthly;
    private LocalDateTime current;
    private LocalDateTime next;

    @BeforeEach
    void beforeEach() {
        time = LocalTime.MIN;
    }

    @Test
    void staticConstructors() {
        assertThrows(new IllegalArgumentException().getClass(), () -> Monthly.on(32));
        assertThrows(new IllegalArgumentException().getClass(), () -> Monthly.on(0));
        monthly = Monthly.on(1);
        assertEquals(1, monthly.getDayOfMonth());
        monthly = Monthly.lastDay();
        assertEquals(31, monthly.getDayOfMonth());
    }

    @Test
    void onFirstDayOfMonth() {
        assertThrows(new IllegalArgumentException().getClass(), () -> Monthly.on(32));

        monthly = Monthly.on(1);
        current = LocalDateTime.of(LocalDate.of(2021, 04, 30), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 05, 01), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 03, 01), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 04, 01), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 02, 15), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 03, 01), next.toLocalDate());
    }

    @Test
    void closeToTheEndOfMonth() {
        monthly = Monthly.on(30);
        current = LocalDateTime.of(LocalDate.of(2021, 02, 15), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 02, 28), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 01, 30), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 02, 28), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 02, 28), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 03, 30), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 03, 30), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 04, 30), next.toLocalDate());
    }

    @Test
    void lastDay() {
        Monthly monthly = Monthly.lastDay();
        current = LocalDateTime.of(LocalDate.of(2021, 04, 30), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 05, 31), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 03, 31), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 04, 30), next.toLocalDate());

        current = LocalDateTime.of(LocalDate.of(2021, 02, 15), time);
        next = monthly.nextEventAfter(current);
        assertEquals(LocalDate.of(2021, 02, 28), next.toLocalDate());
    }

    @Test
    void nextDayEvent_withLastDay() {
        int[] daysNonLeapYear = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int[] daysInLeapYear = new int[] {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        monthly = Monthly.lastDay();
        current = LocalDateTime.of(LocalDate.of(2020, 01, 01), time);
        //leap year
        for (int i = 0; i < 12; i++) {
            next = monthly.nextEventAfter(current);
            assertEquals(daysInLeapYear[i], next.getDayOfMonth());
            current = next;
        }
        //not leap year
        for (int i = 0; i < 12; i++) {
            next = monthly.nextEventAfter(current);
            assertEquals(daysNonLeapYear[i], next.getDayOfMonth());
            current = next;
        }
    }

    @Test
    void nextDayEvent_on30th() {
        int[] daysNonLeapYear = new int[] {30, 28, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30};
        int[] daysInLeapYear = new int[] {30, 29, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30};
        monthly = Monthly.on(30);
        current = LocalDateTime.of(LocalDate.of(2020, 01, 01), time);
        //leap year
        for (int i = 0; i < 12; i++) {
            next = monthly.nextEventAfter(current);
            assertEquals(daysInLeapYear[i], next.getDayOfMonth());
            current = next;
        }
        //not leap year
        for (int i = 0; i < 12; i++) {
            next = monthly.nextEventAfter(current);
            assertEquals(daysNonLeapYear[i], next.getDayOfMonth());
            current = next;
        }
    }

    @Test
    void nextDayEvent_on1st() {
        int[] daysNonLeapYear = new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int[] daysInLeapYear = new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        monthly = Monthly.on(1);
        current = LocalDateTime.of(LocalDate.of(2019, 12, 31), time);
        //leap year
        for (int i = 0; i < 12; i++) {
            next = monthly.nextEventAfter(current);
            assertEquals(daysInLeapYear[i], next.getDayOfMonth());
            current = next;
        }
        //not leap year
        for (int i = 0; i < 12; i++) {
            next = monthly.nextEventAfter(current);
            assertEquals(daysNonLeapYear[i], next.getDayOfMonth());
            current = next;
        }
    }

    @Test
    void testCopy() {
        monthly = Monthly.on(15);
        Monthly copy = monthly.copy();
        current = LocalDateTime.of(LocalDate.of(2019, 12, 31), time);
        assertAll("Testing for the proper copy",
                () -> assertFalse(monthly == copy),
                () -> assertEquals(monthly.getDayOfMonth(), copy.getDayOfMonth()),
                () -> assertEquals(LocalDateTime.of(LocalDate.of(2020, 01, 15), time), copy.nextEventAfter(current))
                );

    }

}