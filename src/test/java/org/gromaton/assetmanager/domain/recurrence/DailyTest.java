package org.gromaton.assetmanager.domain.recurrence;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DailyTest {

    @Test
    void nextEventAfter() {
        Daily daily = new Daily();
        LocalDateTime someTimeStamp = LocalDateTime.now();
        assertEquals(daily.nextEventAfter(someTimeStamp), someTimeStamp.plusHours(24));
    }

    @Test
    void copy() {
        Daily daily = new Daily();
        Daily copy = (Daily) daily.copy();
        assertFalse(daily == copy);
        LocalDateTime someTimeStamp = LocalDateTime.now();
        assertEquals(daily.nextEventAfter(someTimeStamp), copy.nextEventAfter(someTimeStamp));
    }
}