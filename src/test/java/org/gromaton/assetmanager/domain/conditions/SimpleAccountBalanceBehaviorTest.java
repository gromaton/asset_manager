package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.accounts.SimpleAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Random;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class SimpleAccountBalanceBehaviorTest {
    private SimpleAccountBalanceBehavior behavior;
    private SimpleAccount account;

    @BeforeEach
    void beforeEach() {
        account = mock(SimpleAccount.class);
        when(account.getBalance()).thenReturn(new BigDecimal("125.000"));
        behavior = new SimpleAccountBalanceBehavior(account);
    }

    @Test
    void balanceOn() {
        LocalDateTime anyTimeStamp = LocalDateTime.now().minusDays(new Random().nextInt());
        System.out.println(anyTimeStamp);
        assertEquals(new BigDecimal("125.000"), behavior.balanceOn(anyTimeStamp));
    }
}