package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.recurrence.Monthly;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class SavingsAccountConditionsTest {
    BalanceBehavior bb;
    LocalDateTime firstTopUp;
    //LocalDateTime secondTopUp;
    SavingsAccountConditions conditions;

    @BeforeEach
    void beforeEach() {

    }

    @Test
    void calcBalance() {
        bb = mock(BalanceBehavior.class, new CustomAnswer());
        firstTopUp  = LocalDateTime.of(LocalDate.of(2021, 05, 6), LocalTime.MIN);
        conditions = new SavingsAccountConditions("Cond", 0.1, Monthly.on(6));
        //assertEquals(BigDecimal.TEN, bb.balanceOn(firstTopUp));
        assertEquals(BigDecimal.ZERO, bb.balanceOn(firstTopUp.minusMinutes(1)));
        assertEquals(
                new BigDecimal("1008.49"),
                conditions.calcBalance(bb, firstTopUp, firstTopUp.plusMonths(1))
        );
        firstTopUp  = LocalDateTime.of(LocalDate.of(2021, 05, 1), LocalTime.MIN);
        conditions = new SavingsAccountConditions("Cond", 0.1, Monthly.on(6));
        assertEquals(
                new BigDecimal("1001.37"),
                conditions.calcBalance(bb, firstTopUp, firstTopUp.plusMonths(1))
        );
        assertEquals(
                new BigDecimal("1000.00"),
                conditions.calcBalance(bb, firstTopUp.plusDays(6), firstTopUp.plusMonths(1))
        );
    }

    @Test
    void calcBalance_checkIfCapitalizationIsTaken() {
        bb = mock(BalanceBehavior.class, new CustomAnswer());
        firstTopUp  = LocalDateTime.of(LocalDate.of(2021, 05, 1), LocalTime.MIN);
        int balanceBehaviorScale = bb.balanceOn(LocalDateTime.now()).scale();
        conditions = new SavingsAccountConditions("Cond", 0.1, Monthly.on(6));
        BigDecimal balanceAfterFirstPayout = new BigDecimal("1001.37");
        assertEquals(
                balanceAfterFirstPayout,
                conditions.calcBalance(bb, firstTopUp, firstTopUp.plusMonths(1))
        );
        assertEquals(
                balanceAfterFirstPayout.add(balanceAfterFirstPayout.multiply(new BigDecimal(0.1/365)).multiply(BigDecimal.valueOf(31))).setScale(balanceBehaviorScale, BigDecimal.ROUND_HALF_UP),
                //new BigDecimal("1009.874648152"),
                conditions.calcBalance(bb, firstTopUp, firstTopUp.plusMonths(2))
        );
    }

    private class CustomAnswer implements Answer<BigDecimal> {
        @Override
        public BigDecimal answer(InvocationOnMock invocationOnMock) throws Throwable {
            LocalDateTime inputTimeStamp = invocationOnMock.getArgument(0, LocalDateTime.class);
            if (inputTimeStamp.isBefore(firstTopUp)) {
                return BigDecimal.ZERO;
            }
            return new BigDecimal("1000.00");
        }
    }
}