package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BalanceBehaviorFactoryImplTest {
    @Autowired
    List<Account> availableAccounts;
    @Autowired
    BalanceBehaviorFactoryImpl factory;

    @Test
    void createFor() {
        for (Account acc : availableAccounts) {
            assertDoesNotThrow(() -> factory.createFor(acc));
            assertNotNull(factory.createFor(acc));
        }
    }
}