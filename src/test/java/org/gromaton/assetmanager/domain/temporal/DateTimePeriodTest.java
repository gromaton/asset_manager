package org.gromaton.assetmanager.domain.temporal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DateTimePeriodTest {

    private LocalDateTime leftBound;
    private LocalDateTime rightBound;

    @BeforeEach
    void beforeEach() {
        leftBound = LocalDateTime.of(2021, 03, 01, 14, 30, 0);
        rightBound = LocalDateTime.of(2021, 04, 05, 15, 30, 0);
    }

    @Test
    void allUpto() {
        DateTimePeriod dateTimePeriod = DateTimePeriod.allUpto(rightBound);
        assertAll("Test DateRange.allUpto",
                () -> assertTrue(dateTimePeriod.includes(LocalDateTime.MIN)),
                () -> assertFalse(dateTimePeriod.includes(LocalDateTime.MAX))
        );
    }

    @Test
    void untilNow() {
        DateTimePeriod dateTimePeriod = DateTimePeriod.untilNow();
        assertAll("Test DateRange.untilNow",
                () -> assertTrue(dateTimePeriod.includes(LocalDateTime.MIN)),
                () -> assertFalse(dateTimePeriod.includes(LocalDateTime.now().plusNanos(1)))
        );
    }

    @Test
    void includes() {
        DateTimePeriod dateTimePeriod = new DateTimePeriod(leftBound, rightBound);
        LocalDateTime inside = rightBound.minusDays(1);
        LocalDateTime outsideFromRight = rightBound.plusDays(1);
        LocalDateTime outsideFromLeft = leftBound.minusDays(1);
        assertAll("Testing if DateRange includes time stamps",
                () -> assertTrue(dateTimePeriod.includes(leftBound)),
                () -> assertTrue(dateTimePeriod.includes(rightBound)),
                () -> assertTrue(dateTimePeriod.includes(inside)),
                () -> assertFalse(dateTimePeriod.includes(outsideFromLeft)),
                () -> assertFalse(dateTimePeriod.includes(outsideFromRight))
        );
    }

    @Test
    void includesNull() {
        DateTimePeriod dateTimePeriod = new DateTimePeriod(leftBound, rightBound);
        assertFalse(dateTimePeriod.includes(null));
    }
}