package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SimpleOperatingAccountTest {
    OperatingAccount eurAcc;
    AssetType ethType;
    OperatingAccount ethAcc;


    @BeforeEach
    void beforeEach() {
        eurAcc = new SimpleOperatingAccount();
        ethType = new AssetType("ETH", 18, Market.CryptoCurrency);
        ethAcc = new SimpleOperatingAccount(ethType);
    }

    @Test
    void zeroBalance_AfterCreatingNew() {
        assertEquals(new BigDecimal("0.00"), eurAcc.getBalance());
    }

    @Test
    void balanceIsNotZero_AfterOperationAdding() throws Exception {
        eurAcc.add(new SimpleOperation(new Asset("100.00")));
        assertEquals(new BigDecimal("100.00"), eurAcc.getBalance());
        eurAcc.add(new SimpleOperation(new Asset("250.00")));
        assertEquals(new BigDecimal("350.00"), eurAcc.getBalance());
    }

    @Test
    void creatingNotDefaultAssetType() {
        assertEquals(new BigDecimal("0.000000000000000000"), ethAcc.getBalance());
        assertEquals("ETH", ethAcc.getSymbol());
    }

    @Test
    void exceptionOnAddingAnotherTypeOperation() {
        assertThrows(Exception.class, () -> {
            eurAcc.add(new SimpleOperation(new Asset("0.1", ethType)));
        });
        assertDoesNotThrow(() -> {
            eurAcc.add(new SimpleOperation(new Asset("0.1")));
        });
    }

    @Test
    void removingOperationThatHasNoId() throws Exception {
        SimpleOperation operation = new SimpleOperation(new Asset("100.00"));
        eurAcc.add(operation);
        assertEquals(new BigDecimal("100.00"), eurAcc.getBalance());
        eurAcc.remove(operation);
        assertEquals(new BigDecimal("0.00"), eurAcc.getBalance());
    }

    @Test
    void removingOperationWithId() throws Exception {
        SimpleOperation operation = new SimpleOperation(new Asset("100.00"));
        Field opField = operation.getClass().getSuperclass().getSuperclass().getDeclaredField("id");
        opField.setAccessible(true);
        opField.setLong(operation, 1L);
        eurAcc.add(operation);
        assertEquals(new BigDecimal("100.00"), eurAcc.getBalance());

        SimpleOperation newOpButWithSameId = new SimpleOperation(new Asset("150.00"));
        opField.setLong(newOpButWithSameId, 1L);
        assertEquals(true, eurAcc.remove(newOpButWithSameId));
        assertEquals("0", eurAcc.getBalance().stripTrailingZeros().toPlainString());
    }

    @Test
    void thereIsNoFutureOperation_whenGetBalance() throws Exception {
        LocalDateTime today = LocalDateTime.now();
        Operation operationInFuture = new SimpleOperation(new Asset("20", ethType), today.plusDays(1L));
        ethAcc.add(operationInFuture);
        Operation operationInPast = new SimpleOperation(new Asset("10", ethType), today.minusDays(1L));
        ethAcc.add(operationInPast);
        assertEquals("10", ethAcc.getBalance().stripTrailingZeros().toPlainString());
    }

    @Test
    void getBalanceForSpecificDate() throws Exception {
        LocalDateTime today = LocalDateTime.now();
        Operation operationToday = new SimpleOperation(new Asset("10", ethType), today);
        ethAcc.add(operationToday);
        assertEquals(new BigDecimal("0"), ethAcc.getBalance(today.minusDays(2)).stripTrailingZeros());
        Operation operationInFuture = new SimpleOperation(new Asset("50", ethType), today.plusDays(2));
        ethAcc.add(operationInFuture);
        assertEquals("10", ethAcc.getBalance(today.plusDays(1)).stripTrailingZeros().toPlainString());
        assertEquals("60", ethAcc.getBalance(today.plusDays(3)).stripTrailingZeros().toPlainString());
    }

    @Test
    void addTheSameOperation() throws Exception {
        Operation operation = new SimpleOperation(new Asset("10", ethType));
        ethAcc.add(operation);
        ethAcc.add(operation);
        List<Operation> operationList = ethAcc.getOperations();
        assertEquals(1, operationList.size());
    }

    @Test
    void addOperationWithSameAndAnotherId() throws Exception {
        SimpleOperation operation = new SimpleOperation(new Asset("100.00"));
        Field opField = operation.getClass().getSuperclass().getSuperclass().getDeclaredField("id");
        opField.setAccessible(true);
        opField.setLong(operation, 1L);
        eurAcc.add(operation);
        assertEquals(new BigDecimal("100.00"), eurAcc.getBalance());

        //add same id
        SimpleOperation newOperation = new SimpleOperation(new Asset("150.00"));
        opField.setLong(newOperation, 1L);
        assertEquals(false, eurAcc.add(newOperation));
        assertEquals(new BigDecimal("100.00"), eurAcc.getBalance());

        //add another id
        opField.setLong(newOperation, 2L);
        assertEquals(true, eurAcc.add(newOperation));
        assertEquals(new BigDecimal("250.00"), eurAcc.getBalance());
    }

    @Test
    void getOperations() throws Exception {
        Operation operation1 = new SimpleOperation(new Asset("10", ethType));
        Operation operation2 = new SimpleOperation(new Asset("10", ethType));
        ethAcc.add(operation1);
        ethAcc.add(operation2);
        assertEquals(2, ethAcc.getOperations().size());
    }
}