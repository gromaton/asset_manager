package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;
import org.gromaton.assetmanager.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class SimpleOperatingAccountDBTest {
    @Autowired
    private AccountService accountService;
    private AssetType eurType;
    private SimpleOperatingAccount eurOpAcc;
    private SimpleOperation operation;

    @BeforeEach
    void beforeEach() {
        eurType = new AssetType("EUR", 8, Market.Money);
        eurOpAcc = new SimpleOperatingAccount(eurType);
        operation = new SimpleOperation(new Asset("50", eurType));
    }

    @Test
    void saveOperatingAccountIntoDB() {
        assertEquals(0, eurOpAcc.getId());
        accountService.save(eurOpAcc);
        assertNotEquals(0, eurOpAcc.getId());
    }

    @Test
    void addedOperationIsSaved() throws Exception {
        eurOpAcc.add(operation);
        assertEquals(0, operation.getId());
        accountService.save(eurOpAcc);
        assertNotEquals(0, operation.getId());
    }

    @Test
    void readOperatingAccountWithOperationsFromDB() throws Exception {
        eurOpAcc.add(operation);
        Operation secondOperation = new SimpleOperation(new Asset("70", eurType));
        eurOpAcc.add(secondOperation);
        accountService.save(eurOpAcc);
        long eurOpAccId = eurOpAcc.getId();
        List<Account> accountList = accountService.findAll();
        SimpleOperatingAccount opAccFromDB;
        opAccFromDB = accountList.stream().filter(acc -> acc instanceof SimpleOperatingAccount).map(acc -> (SimpleOperatingAccount)acc).collect(Collectors.toList())
                .stream().filter(acc -> acc.getId() == eurOpAccId).findFirst().orElse(null);
        assertAll("Checking if OperatingAccount has been read from DB and it has 2 operations",
                () -> assertNotNull(opAccFromDB),
                () -> assertEquals(2, opAccFromDB.getOperations().size())
        );
    }

    @Test
    void deleteOperationFromOperationList() throws Exception {
        eurOpAcc.add(operation);
        SimpleOperation secondOperation = new SimpleOperation(new Asset("70", eurType));
        eurOpAcc.add(secondOperation);
        accountService.save(eurOpAcc);
        long eurOpAccId = eurOpAcc.getId();
        eurOpAcc.remove(operation);
        accountService.save(eurOpAcc);
        List<Account> accountList = accountService.findAll();
        SimpleOperatingAccount opAccFromDB;
        opAccFromDB = accountList.stream().filter(acc -> acc instanceof SimpleOperatingAccount).map(acc -> (SimpleOperatingAccount)acc).collect(Collectors.toList())
                .stream().filter(acc -> acc.getId() == eurOpAccId).findFirst().orElse(null);
        assertAll("Checking that only one the second operation in the operation list",
                () -> assertEquals(1, opAccFromDB.operations.size()),
                () -> assertEquals(new Asset("70.00", eurType), opAccFromDB.getOperations().get(0).getAsset())
        );
    }

    @Test
    void deleteOperatingAccountFromDB() {
        assertAll("Checking if OperatingAccount is deleted from the DB",
                () -> {
                    accountService.save(eurOpAcc);
                    assertNotEquals(0, eurOpAcc.getId());
                },
                () -> {
                    Set<Account> setOfOneAccount = new HashSet<>();
                    setOfOneAccount.add(eurOpAcc);
                    List<Account> accountsInDB = accountService.findAll();
                    long accQuantityInDB = accountsInDB.size();
                    accountService.deleteAll(setOfOneAccount);
                    assertEquals(accQuantityInDB - 1, accountService.findAll().size());
                }
        );
    }
}