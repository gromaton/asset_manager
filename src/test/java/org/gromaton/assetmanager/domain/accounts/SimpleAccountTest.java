package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class SimpleAccountTest {

    private SimpleAccount acc;

    @BeforeEach
    void beforeEach() {
        acc = new SimpleAccount("Name", new Asset());
    }
    @Test
    void getBalance() {
        assertEquals(BigDecimal.ZERO.setScale(2), acc.getBalance());
    }

    @Test
    void getName() {
        assertEquals("Name", acc.getName());
    }
}