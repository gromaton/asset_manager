package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.operations.visitors.OperationCopyFactory;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class OperationCopyFactoryTest {
    private AssetType btcType;
    private Asset btcAsset;
    private LocalDateTime timeStamp;

    @BeforeEach
    void beforeEach() {
        btcType = new AssetType("BTC", 8, Market.CryptoCurrency);
        btcAsset = new Asset("0.02", btcType);
        timeStamp = LocalDateTime.of(LocalDate.of(2021, 03, 31), LocalTime.MIN);
    }

    @Test
    void simpleOperationCopy() {
        Operation orig = new SimpleOperation(btcAsset, timeStamp, "income");
        Operation copy = OperationCopyFactory.copy(orig);
        assertAll("Checking references are not equal, but classes and all fields are equal",
                () -> assertTrue(orig != copy),
                () -> assertEquals(SimpleOperation.class, copy.getClass()),
                () -> assertEquals(btcAsset, copy.getAsset()),
                () -> assertEquals(timeStamp, copy.getTimeStamp()),
                () -> assertEquals("income", copy.getDescription())
        );

    }

    @Test
    void recurringSimpleOperationCopy() {
        SimpleOperation operation = new SimpleOperation(btcAsset, timeStamp, "income");
        RecurrenceCondition condition = mock(RecurrenceCondition.class);
        when(condition.nextEventAfter(timeStamp)).thenReturn(timeStamp.plusMonths(1));
        RecurringOperation orig = new RecurringOperation(operation, condition);
        assertDoesNotThrow(() -> (RecurringOperation) OperationCopyFactory.copy(orig));
        RecurringOperation copy = (RecurringOperation) OperationCopyFactory.copy(orig);

        assertAll("Checking references are not equal, but classes and all fields are equal",
                () -> assertTrue(orig != copy),
                () -> assertEquals(RecurringOperation.class, copy.getClass()),
                () -> assertEquals(btcAsset, copy.getBaseOperation().getAsset()),
                () -> assertEquals(timeStamp, copy.getBaseOperation().getTimeStamp()),
                () -> assertEquals("income", copy.getBaseOperation().getDescription()),
                () -> assertFalse(orig.getBaseOperation() == copy.getBaseOperation()),
                () -> assertFalse(orig.getRecurrenceCondition() == copy.getRecurrenceCondition())
        );
    }

}