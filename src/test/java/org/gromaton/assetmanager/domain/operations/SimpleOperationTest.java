package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


class SimpleOperationTest {

    private AssetType usdType;
    private AssetType btcType;
    private Asset usdAsset;
    private Asset btcAsset;

    @BeforeEach
    void beforeEach() {
        usdType = new AssetType("USD", 2, Market.Money);
        btcType = new AssetType("BTC", 8, Market.CryptoCurrency);
        usdAsset = new Asset("125", usdType);
        btcAsset = new Asset("0.1", btcType);
    }

    @Test
    void gettingAsset() {
        Operation operation = new SimpleOperation(usdAsset);
        assertEquals(new BigDecimal("125.00"), operation.getAsset().amount());
        assertEquals("USD", operation.getAsset().type().symbol());
        assertEquals(2, operation.getAsset().type().scale());

        operation = new SimpleOperation(btcAsset);
        assertEquals(new BigDecimal("0.10000000"), operation.getAsset().amount());
        assertEquals("BTC", operation.getAsset().type().symbol());
        assertEquals(8, operation.getAsset().type().scale());
    }

    @Test
    void getTimeStamp() {
        Operation operation = new SimpleOperation(usdAsset);
        assertNotNull(operation.getTimeStamp());
        LocalDateTime timeStamp = LocalDateTime.now();
        operation = new SimpleOperation(usdAsset, timeStamp);
        assertEquals(timeStamp, operation.getTimeStamp());
    }

    @Test
    void getDateAndTime() {
        LocalDateTime timeStamp = LocalDateTime.now();
        Operation operation = new SimpleOperation(usdAsset, timeStamp);
        assertAll("Testing getDate and getTime as convertation from LocalDateTime",
                () -> assertEquals(timeStamp.toLocalDate(), operation.getDate()),
                () -> assertEquals(timeStamp.toLocalTime(), operation.getTime())
        );
    }

    @Test
    void equalsWithoutId() {
        LocalDateTime timestamp = LocalDateTime.now();
        Operation operation1 = new SimpleOperation(usdAsset, timestamp);
        Operation operation2 = new SimpleOperation(new Asset("125", usdType), timestamp);
        assertNotEquals(operation1, operation2);
        operation2 = new SimpleOperation(usdAsset, timestamp.plusMinutes(1));
        assertNotEquals(operation1, operation2);
        operation2 = new SimpleOperation(new Asset("100"), timestamp);
        assertNotEquals(operation1, operation2);
        operation2 = new SimpleOperation(new Asset("125.0", btcType), timestamp);
        assertNotEquals(operation1, operation2);
    }

    @Test
    void equalsWithNotNullId() throws NoSuchFieldException, IllegalAccessException {
        LocalDateTime timestamp = LocalDateTime.now();
        Operation operation = new SimpleOperation(usdAsset, timestamp);

        Field operationIdField = operation.getClass().getSuperclass().getSuperclass().getDeclaredField("id");
        operationIdField.setAccessible(true);
        operationIdField.setLong(operation, 1L);
        Operation operation2 = new SimpleOperation(usdAsset, timestamp);
        assertNotEquals(operation, operation2);

        Field operation2IdField = operation2.getClass().getSuperclass().getSuperclass().getDeclaredField("id");
        operation2IdField.setAccessible(true);
        operation2IdField.setLong(operation2, 1L);
        assertEquals(operation, operation2);
        operation2IdField.setLong(operation2, 2L);
        assertNotEquals(operation, operation2);
    }

    @Test
    void getDescription() {
        Operation operation = new SimpleOperation(usdAsset, LocalDateTime.now(), "test");
        assertEquals("test", operation.getDescription());
    }

    @Test
    void cannotCreateNullDescription() {
        Operation operation = new SimpleOperation(usdAsset, LocalDateTime.now(), null);
        assertEquals("", operation.getDescription());
    }

    @Test
    void cannotCreateNullTimeStamp() {
        assertThrows(IllegalArgumentException.class, () -> new SimpleOperation(usdAsset, null));
    }

    @Test
    void cannotCreateNullAsset() {
        assertThrows(IllegalArgumentException.class, () -> new SimpleOperation(null));
    }
}