package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class RecurringOperationTest {

    private AssetType btcType;
    private Asset btcAsset;

    @BeforeEach
    void beforeEach() {
        btcType = new AssetType("BTC", 8, Market.CryptoCurrency);
        btcAsset = new Asset("0.02", btcType);
    }

    @Test
    void iterator_WhenBaseOperationTimeStampIsNotInCondition() {
        LocalDateTime timeStamp = LocalDateTime.of(LocalDate.of(2021, 03, 11), LocalTime.MIN);
        SimpleOperation operation = new SimpleOperation(btcAsset, timeStamp, "income");
        RecurrenceCondition condition = mock(RecurrenceCondition.class);

        when(condition.nextEventAfter(timeStamp)).thenReturn(timeStamp.withDayOfMonth(31));
        when(condition.nextEventAfter(LocalDateTime.of(LocalDate.of(2021, 03, 31), LocalTime.MIN))).thenReturn(timeStamp.withDayOfMonth(31).plusMonths(1));
        when(condition.copy()).thenReturn(condition);
        RecurringOperation recurringOperation = new RecurringOperation(operation, condition);
        Iterator<Operation> iterator = recurringOperation.iterator();
        Operation nextOperation = iterator.next();
        assertEquals(timeStamp, nextOperation.getTimeStamp());
        nextOperation = iterator.next();
        assertEquals(timeStamp.withDayOfMonth(31), nextOperation.getTimeStamp());
        nextOperation = iterator.next();
        assertEquals(LocalDateTime.of(LocalDate.of(2021, 04, 30), LocalTime.MIN), nextOperation.getTimeStamp());

    }
}