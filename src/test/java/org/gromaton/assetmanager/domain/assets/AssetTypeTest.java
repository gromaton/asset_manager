package org.gromaton.assetmanager.domain.assets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AssetTypeTest {
    AssetType assetType;
    @BeforeEach
    void beforeEach() {
        assetType = new AssetType("BTC", 8, Market.CryptoCurrency);
    }

    @Test
    void symbol() {
        assertEquals("BTC", assetType.symbol());
    }

    @Test
    void scale() {
        assertEquals(8, assetType.scale());
    }

    @Test
    void marketType() {
        assertEquals(Market.CryptoCurrency, assetType.marketType());
    }

    @Test
    void testEquals() {
        AssetType anotherAssetType = new AssetType("BTC", 8, Market.CryptoCurrency);
        assertEquals(assetType, anotherAssetType);
        anotherAssetType = new AssetType("ETH", 8, Market.CryptoCurrency);
        assertNotEquals(assetType, anotherAssetType);
        anotherAssetType = new AssetType("BTC", 9, Market.CryptoCurrency);
        assertNotEquals(assetType, anotherAssetType);
        anotherAssetType = new AssetType("BTC", 8, Market.Money);
        assertNotEquals(assetType, anotherAssetType);
    }
}