package org.gromaton.assetmanager.domain.assets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class AssetTest {
    Asset btcAsset;
    Asset usdAsset;
    AssetType btcType;
    AssetType usdType;
    @BeforeEach
    void beforeEach() {
        btcType = new AssetType("BTC", 8, Market.CryptoCurrency);
        usdType = new AssetType("USD", 2, Market.Money);
        btcAsset = new Asset(new BigDecimal("1.234"), btcType);
        usdAsset = new Asset(new BigDecimal("125"), usdType);
    }

    @Test
    void getters() {
        assertAll("Testing AssetImpl getters",
                () -> assertEquals(new BigDecimal("1.23400000"), btcAsset.amount()),
                () -> assertEquals(btcType, btcAsset.type())
        );
    }

    @Test
    void equals() {
        assertAll("Testing different variants of equality",
                () -> assertTrue(btcAsset.equals(btcAsset)),
                () -> {
                    Asset newBtcAsset = new Asset("1.2340", btcType);
                    assertEquals(btcAsset, newBtcAsset);
                },
                () -> {
                    Asset newBtcAsset = new Asset("1.234", usdType);
                    assertNotEquals(btcAsset, newBtcAsset);
                },
                () -> {
                    Asset newUsdAsset = new Asset("125.00000", usdType);
                    assertEquals(usdAsset, newUsdAsset);
                });
    }
}