package org.gromaton.assetmanager.domain.investments;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.mockito.Mockito.*;

public class PotentialInvestmentTest {
    PotentialInvestment investment;
    Account account;
    AbstractConditions conditions;

    @BeforeEach
    void beforeEach() {
        account = mock(Account.class);
        conditions = mock(AbstractConditions.class);
    }

    @Test
    public void predict() {
        LocalDateTime openTimeStamp = LocalDateTime.now().plusDays(17);
        LocalDate target = openTimeStamp.plusDays(44).toLocalDate();
        investment = new PotentialInvestment(account, conditions, openTimeStamp);
        investment.predict(target);
        verify(conditions).calcBalance(any(), eq(investment.getOpenTimeStamp()), eq(LocalDateTime.of(target, LocalTime.MIN)));
    }

}