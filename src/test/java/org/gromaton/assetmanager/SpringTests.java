package org.gromaton.assetmanager;

import org.gromaton.assetmanager.domain.accounts.Account;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class SpringTests {
    @Autowired
    List<Account> accountList;
    @Autowired
    ApplicationContext context;

    @Test
    void gettingAccountList() {
        System.out.println(accountList);
        assertEquals(2, accountList.size());
        assertEquals("Operating Account", accountList.get(1).typeTitle());
        assertEquals("Simple Account", accountList.get(0).typeTitle());
    }
}
