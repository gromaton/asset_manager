package org.gromaton.assetmanager.javatimeextension;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class EndOfDayTest {
    @Test
    void testEndOfDayAdjuster_forLocalDateTime() {
        LocalDateTime timeStamp = LocalDateTime.of(LocalDate.of(2021, 01, 01), LocalTime.MIN);
        timeStamp = timeStamp.with(new EndOfDay());
        assertEquals(LocalTime.MAX, timeStamp.toLocalTime());
    }
}