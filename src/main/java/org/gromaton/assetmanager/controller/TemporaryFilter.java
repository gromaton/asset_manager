package org.gromaton.assetmanager.controller;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.Enumeration;

//This Filter is just for testing how it works with vaadin
@Component
public class TemporaryFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println(">>>>>>>>Filter initialization");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println(">>>>>Filter working");
        Enumeration<String> names = servletRequest.getParameterNames();
        String name;
        while (names.hasMoreElements()) {
            name = names.nextElement();
            System.out.println(name);
            System.out.println(servletRequest.getParameterValues(name)[0]);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    @Bean
    public FilterRegistrationBean <TemporaryFilter> filterRegistrationBean() {
        FilterRegistrationBean <TemporaryFilter> registrationBean = new FilterRegistrationBean();
        TemporaryFilter temporaryFilter = new TemporaryFilter();

        registrationBean.setFilter(temporaryFilter);
        registrationBean.addUrlPatterns("/opportunities");
        registrationBean.setOrder(1); //set precedence
        System.out.println(">>>>Filter registered");
        return registrationBean;
    }
}
