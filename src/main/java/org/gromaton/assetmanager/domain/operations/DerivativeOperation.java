package org.gromaton.assetmanager.domain.operations;

public interface DerivativeOperation extends Operation {
    Operation getOrigin();
}
