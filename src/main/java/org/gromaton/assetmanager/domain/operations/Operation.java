package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.IEntity;
import org.gromaton.assetmanager.domain.operations.visitors.OperationVisitor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public interface Operation extends IEntity {
    String typeTitle();
    Asset getAsset();
    LocalDateTime getTimeStamp();
    default LocalDate getDate() {
        return getTimeStamp().toLocalDate();
    };
    default LocalTime getTime() {
        return getTimeStamp().toLocalTime();
    };
    String getDescription();
    void setAmount(BigDecimal amount);
    void setAssetType(AssetType assetType);
    void setTimeStamp(LocalDateTime timeStamp);
    void setDate(LocalDate date);
    void setDescription(String description);
    void accept(OperationVisitor visitor);
}
