package org.gromaton.assetmanager.domain.operations.filters;

import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.temporal.DateTimePeriod;

import java.time.LocalDateTime;

public class DateRangeFilter implements OperationFilter {
    DateTimePeriod dateTimePeriod;

    public DateRangeFilter(LocalDateTime start, LocalDateTime end) {
        this.dateTimePeriod = new DateTimePeriod(start, end);
    }

    @Override
    public boolean isConform(Operation operation) {
        return dateTimePeriod.includes(operation.getTimeStamp());
    }
}
