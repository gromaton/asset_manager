package org.gromaton.assetmanager.domain.operations.visitors;

import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;
import org.gromaton.assetmanager.domain.temporal.DateTimePeriod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ViewOperations implements OperationVisitor {
    private Collection<Operation> entities;
    private List<Operation> viewOperations = new ArrayList<>();
    private DateTimePeriod dateTimePeriod;

    public ViewOperations(Collection<Operation> entities) {
        this.entities = entities;
        this.dateTimePeriod = DateTimePeriod.untilNow();
    }

    public List<Operation> getList(DateTimePeriod operationPeriod) {
        if (entities != null && !entities.isEmpty()) {
            fillViewOperations(operationPeriod);
        } else {
            viewOperations.clear();
        }
        return viewOperations;
    }

    public void update() {
        fillViewOperations(this.dateTimePeriod);
    }

    @Override
    public void visit(SimpleOperation operation) {
        if (dateTimePeriod.includes(operation.getTimeStamp())) {
            viewOperations.add(operation);
        }
    }

    @Override
    public void visit(RecurringOperation operation) {
        for (Operation next : operation) {
            if (next.getTimeStamp().isAfter(dateTimePeriod.getEnd())) {
                break;
            }
            if (dateTimePeriod.includes(next.getTimeStamp()))
                viewOperations.add(next);
        }
    }

    @Override
    public void visit(RecurringOperation.RecurringOperationDerivative operation) {
        //no action yet required, because this operation type was not intended to be persisted
    }

    private void fillViewOperations(DateTimePeriod period) {
        this.dateTimePeriod = period;
        viewOperations.clear();
        for (Operation operation : entities) {
            operation.accept(this);
        }
        Collections.sort(viewOperations, (op1, op2) -> op2.getTimeStamp().compareTo(op1.getTimeStamp()));
    }
}
