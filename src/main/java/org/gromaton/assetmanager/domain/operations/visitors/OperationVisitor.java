package org.gromaton.assetmanager.domain.operations.visitors;

import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;

public interface OperationVisitor {
    void visit(SimpleOperation operation);
    void visit(RecurringOperation operation);
    void visit(RecurringOperation.RecurringOperationDerivative operation);
}
