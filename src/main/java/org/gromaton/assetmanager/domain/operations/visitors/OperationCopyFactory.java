package org.gromaton.assetmanager.domain.operations.visitors;

import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;

//ToDo: think about to change this factory to copy method or copying constructor, what +/-
public class OperationCopyFactory implements OperationVisitor {
    public static Operation copy(Operation operation) {
        return new OperationCopyFactory().createCopy(operation);
    }

    Operation newOperation;

    @Override
    public void visit(SimpleOperation operation) {
        SimpleOperation simpleOperation = new SimpleOperation();
        simpleOperation.setAmount(operation.getAsset().amount());
        simpleOperation.setAssetType(operation.getAsset().type());
        simpleOperation.setTimeStamp(operation.getTimeStamp());
        simpleOperation.setDescription(operation.getDescription());
        newOperation = simpleOperation;
    }

    @Override
    public void visit(RecurringOperation operation) {
        Operation baseOperationCopy = this.createCopy(operation.getBaseOperation());
        newOperation = new RecurringOperation(baseOperationCopy, operation.getRecurrenceCondition().copy());
    }

    @Override
    public void visit(RecurringOperation.RecurringOperationDerivative operation) {
        newOperation = ((RecurringOperation) operation.getOrigin()).new RecurringOperationDerivative();
        newOperation.setTimeStamp(operation.getTimeStamp());
        newOperation.setDescription(operation.getDescription());
        newOperation.setAmount(operation.getAsset().amount());
        newOperation.setAssetType(operation.getAsset().type());
    }

    private Operation createCopy(Operation operation) {
        if (operation == null) {
            throw new IllegalArgumentException("Null: Cannot create copy of a null operation");
        }
        operation.accept(this);
        return this.newOperation;
    }
}
