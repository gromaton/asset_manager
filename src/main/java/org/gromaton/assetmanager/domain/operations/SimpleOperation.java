package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.operations.visitors.OperationVisitor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

@Component
@Entity
@Scope("prototype")
public class SimpleOperation extends AbstractOperation implements Operation {
    //Keep it here for a while in order to remember don't do like this
    /*public final static Asset DEFAULT_ASSET = new Asset();
    private final static String DEFAULT_DESCRIPTION = "";*/

    @Embedded
    @NotNull
    private Asset asset;
    private LocalDateTime timeStamp;
    private String description;

    public SimpleOperation() {
        this(new Asset(), LocalDateTime.of(LocalDate.now(), LocalTime.MIN), "");
    }

    public SimpleOperation(Asset asset) {
        this(asset, LocalDateTime.of(LocalDate.now(), LocalTime.MIN), "");
    }

    public SimpleOperation(Asset asset, LocalDateTime timeStamp) {
        this(asset, timeStamp, "");
    }

    public SimpleOperation(Asset asset, LocalDateTime timeStamp, String description) {
        if (asset == null || timeStamp == null) {
            throw new IllegalArgumentException("Asset or timeStamp cannot be null in SimpleOperation");
        }
        this.asset = asset;
        this.timeStamp = timeStamp;
        this.description = description == null ? "" : description;
    }

    @Override
    public String typeTitle() {
        return "Simple Operation";
    }

    @Override
    public Asset getAsset() {
        return asset;
    }

    @Override
    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.asset = new Asset(amount, this.asset.type());
    }

    @Override
    public void setAssetType(AssetType assetType) {
        if (assetType == null) assetType = new AssetType();
        this.asset = new Asset(this.asset.amount(), assetType);
    }

    @Override
    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public void setDate(LocalDate date) {
        this.timeStamp = LocalDateTime.of(date, getTime());
    }

    @Override
    public void setDescription(String description) {
        if (description != null)
            this.description = description;
    }

    @Override
    public void accept(OperationVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return timeStamp.toString() + ": " + asset.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleOperation)) return false;
        SimpleOperation operation = (SimpleOperation) o;
        return getId() != 0 && operation.getId() != 0 && getId() == operation.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
