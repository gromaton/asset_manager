package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.operations.visitors.OperationCopyFactory;
import org.gromaton.assetmanager.domain.operations.visitors.OperationVisitor;
import org.gromaton.assetmanager.domain.recurrence.AbstractRecurrenceCondition;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;

@Entity
public class RecurringOperation extends AbstractOperation implements Operation, Iterable<Operation> {
    @OneToOne(targetEntity = AbstractOperation.class, cascade = {CascadeType.ALL})
    private Operation baseOperation;
    @OneToOne(targetEntity = AbstractRecurrenceCondition.class, cascade = {CascadeType.ALL}, orphanRemoval = true)
    private RecurrenceCondition recurrenceCondition;

    public RecurringOperation(Operation operation, RecurrenceCondition recurrenceCondition) {
        this.baseOperation = operation;
        this.recurrenceCondition = recurrenceCondition;
    }

    protected RecurringOperation() {
    }

    public Operation getBaseOperation() {
        return baseOperation;
    }

    public void setBaseOperation(Operation baseOperation) {
        this.baseOperation = baseOperation;
    }

    public RecurrenceCondition getRecurrenceCondition() {
        return recurrenceCondition;
    }

    public void setRecurrenceCondition(RecurrenceCondition recurrenceCondition) {
        this.recurrenceCondition = recurrenceCondition;
    }

    @Override
    public String typeTitle() {
        return "Recurring " + baseOperation.typeTitle();
    }

    @Override
    public Asset getAsset() {
        return baseOperation.getAsset();
    }

    @Override
    public LocalDateTime getTimeStamp() {
        return baseOperation.getTimeStamp();
    }

    @Override
    public String getDescription() {
        return baseOperation.getDescription();
    }

    @Override
    public void setAmount(BigDecimal amount) {
        baseOperation.setAmount(amount);
    }

    @Override
    public void setAssetType(AssetType assetType) {
        baseOperation.setAssetType(assetType);
    }

    @Override
    public void setTimeStamp(LocalDateTime timeStamp) {
        baseOperation.setTimeStamp(timeStamp);
    }

    @Override
    public void setDate(LocalDate date) {
        baseOperation.setDate(date);
    }

    @Override
    public void setDescription(String description) {
        baseOperation.setDescription(description);
    }

    @Override
    public void accept(OperationVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Iterator<Operation> iterator() {
        return new IteratorImpl();
    }

    //ToDo: Concurrent modification exception to add
    private class IteratorImpl implements Iterator<Operation> {
        private Operation next;

        private IteratorImpl() {
//            next = RecurringOperation.this.new RecurringOperationDerivative();
            next = RecurringOperation.this;
        }

        @Override
        public boolean hasNext() {
            return next != null;
        }

        @Override
        public Operation next() {
            Operation result = next;
            next = createNextOperation(next);
            return result;
        }

        private Operation createNextOperation(Operation current) {
            LocalDateTime nextTimeStamp = RecurringOperation.this.getRecurrenceCondition().nextEventAfter(current.getTimeStamp());
            if (nextTimeStamp == null) {
                return null;
            }
            if (current == RecurringOperation.this) {
                next = RecurringOperation.this.new RecurringOperationDerivative();
            } else {
                next = OperationCopyFactory.copy(current);
            }
            next.setTimeStamp(nextTimeStamp);
            return next;
        }
    }

    public class RecurringOperationDerivative implements DerivativeOperation {
        private RecurringOperation origin;
        private Operation derivative;

        public RecurringOperationDerivative() {
            this.origin = RecurringOperation.this;
            derivative = OperationCopyFactory.copy(origin.getBaseOperation());
        }

        @Override
        public RecurringOperation getOrigin() {
            return origin;
        }

        @Override
        public String typeTitle() {
            return getClass().getSimpleName();
        }

        @Override
        public Asset getAsset() {
            return derivative.getAsset();
        }

        @Override
        public LocalDateTime getTimeStamp() {
            return derivative.getTimeStamp();
        }

        @Override
        public String getDescription() {
            return derivative.getDescription();
        }

        @Override
        public void setAmount(BigDecimal amount) {
            derivative.setAmount(amount);
        }

        @Override
        public void setAssetType(AssetType assetType) {
            derivative.setAssetType(assetType);
        }

        @Override
        public void setTimeStamp(LocalDateTime timeStamp) {
            derivative.setTimeStamp(timeStamp);
        }

        @Override
        public void setDate(LocalDate date) {
            derivative.setDate(date);
        }

        @Override
        public void setDescription(String description) {
            derivative.setDescription(description);
        }

        @Override
        public void accept(OperationVisitor visitor) {
            visitor.visit(this);
        }
    }
}
