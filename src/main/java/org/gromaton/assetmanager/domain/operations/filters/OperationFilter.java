package org.gromaton.assetmanager.domain.operations.filters;

import org.gromaton.assetmanager.domain.operations.Operation;

public interface OperationFilter {
    boolean isConform(Operation operation);
}
