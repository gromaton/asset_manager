package org.gromaton.assetmanager.domain.operations;

import org.gromaton.assetmanager.domain.EntityWithIdLong;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractOperation extends EntityWithIdLong implements Operation {

}
