package org.gromaton.assetmanager.domain.investments;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.Predictable;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.domain.conditions.BalanceBehavior;
import org.gromaton.assetmanager.domain.conditions.BalanceBehaviorFactoryImpl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class PotentialInvestment extends AbstractInvestment implements Predictable {
    public PotentialInvestment(Account account, AbstractConditions conditions, LocalDateTime impliedOpenDate) {
        super(account, conditions, impliedOpenDate);
    }

    public PotentialInvestment(Account account, AbstractConditions conditions) {
        super(account, conditions);
    }

    @Override
    public BigDecimal predict(LocalDate targetDate) {
        BalanceBehavior balanceBehavior = new BalanceBehaviorFactoryImpl().createFor(getAccount());
        LocalDateTime end = LocalDateTime.of(targetDate, LocalTime.MIN);
        return getConditions().calcBalance(balanceBehavior, getOpenTimeStamp(), end);
    }
}
