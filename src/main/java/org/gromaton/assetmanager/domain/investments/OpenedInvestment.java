package org.gromaton.assetmanager.domain.investments;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.Predictable;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class OpenedInvestment extends AbstractInvestment implements Predictable {

    private LocalDateTime lastPayoutTimeStamp;

    public OpenedInvestment(Account account, AbstractConditions conditions, LocalDateTime openTimeStamp) {
        super(account, conditions, openTimeStamp);
        this.lastPayoutTimeStamp = openTimeStamp;
    }

    public OpenedInvestment(Account account, AbstractConditions conditions) {
        this(account, conditions, LocalDateTime.now());
    }

    @Override
    public BigDecimal predict(LocalDate targetDate) {
        return BigDecimal.ZERO; //tmp
        //return getBalance().add(getConditions().predictProfit(getAccount().getBalance(), lastPayoutDate, targetDate)).setScale(getBalance().scale(), Rounding.DEFAULT_MODE);
//        return getBalance() + getConditions().predictProfit(getAccount().getBalance(), lastPayoutDate, targetDate);
    }
}