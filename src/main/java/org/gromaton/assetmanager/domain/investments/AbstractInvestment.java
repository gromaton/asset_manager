package org.gromaton.assetmanager.domain.investments;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public abstract class AbstractInvestment {
    private Account account;
    private AbstractConditions conditions;

    private LocalDateTime openTimeStamp;

    public AbstractInvestment(Account account, AbstractConditions conditions, LocalDateTime openTimeStamp) {
        this.account = account;
        this.conditions = conditions;
        this.openTimeStamp = openTimeStamp;
    }

    public AbstractInvestment(Account account, AbstractConditions conditions) {
        this(account, conditions, LocalDateTime.now());
    }

    public Account getAccount() {
        return account;
    }

    public AbstractConditions getConditions() {
        return conditions;
    }

    public LocalDateTime getOpenTimeStamp() {
        return openTimeStamp;
    }

    public BigDecimal getBalance() {
        return account.getBalance();
    }
}
