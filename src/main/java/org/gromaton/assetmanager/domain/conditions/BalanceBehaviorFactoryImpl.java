package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.accounts.OperatingAccount;
import org.gromaton.assetmanager.domain.accounts.SimpleAccount;
import org.springframework.stereotype.Component;

@Component
public class BalanceBehaviorFactoryImpl implements BalanceBehaviorFactory {

    @Override
    public BalanceBehavior createFor(Account account) {
        if (account instanceof SimpleAccount) {
            return new SimpleAccountBalanceBehavior((SimpleAccount) account);
        }
        if (account instanceof OperatingAccount) {
            return new OperatingAccountBalanceBehavior((OperatingAccount) account);
        }
        return null;
    }
}
