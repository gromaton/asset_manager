package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.accounts.OperatingAccount;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class OperatingAccountBalanceBehavior implements BalanceBehavior {
    private final OperatingAccount account;

    public OperatingAccountBalanceBehavior(OperatingAccount account) {
        this.account = account;
    }

    @Override
    public BigDecimal balanceOn(LocalDateTime timeStamp) {
        return account.getBalance(timeStamp);
    }
}
