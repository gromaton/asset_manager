package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.accounts.SimpleAccount;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class SimpleAccountBalanceBehavior implements BalanceBehavior {
    private final SimpleAccount account;

    public SimpleAccountBalanceBehavior(SimpleAccount account) {
        this.account = account;
    }

    @Override
    public BigDecimal balanceOn(LocalDateTime timeStamp) {
        return account.getBalance();
    }
}
