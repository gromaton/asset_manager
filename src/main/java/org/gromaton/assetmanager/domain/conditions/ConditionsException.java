package org.gromaton.assetmanager.domain.conditions;

public class ConditionsException extends Exception {
    public ConditionsException() {
    }

    public ConditionsException(String message) {
        super(message);
    }

    public ConditionsException(String message, Throwable cause) {
        super(message, cause);
    }
}

