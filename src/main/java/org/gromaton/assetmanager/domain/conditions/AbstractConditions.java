package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.EntityWithIdLong;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractConditions extends EntityWithIdLong {
    private String title;

    protected AbstractConditions() {

    }

    public AbstractConditions(AbstractConditions conditions) {
        this.title = conditions.title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public abstract BigDecimal calcBalance(BalanceBehavior balanceBehavior, LocalDateTime start, LocalDateTime end);
}
