package org.gromaton.assetmanager.domain.conditions;

public enum ConditionsType {
    CommonSavingsAccount(SavingsAccountConditions.class, "Common Savings Account"),
    TermDeposit(AbstractConditions.class, "Term Deposit");

    private final Class conditionsClass;
    private final String name;

    private ConditionsType(Class<? extends AbstractConditions> conditionsClass, String name) {
        this.conditionsClass = conditionsClass;
        this.name = name;
    }

    public Class getConditionsClass() {
        return conditionsClass;
    }

    public String getName() {
        return name;
    }

    public static ConditionsType getForClass(Class clazz) {
        for (ConditionsType type : values()) {
            if (type.getConditionsClass() == clazz) {
                return type;
            }
        }
        throw new RuntimeException("No such ConditionsType for class " + clazz.getName());
    }
}
