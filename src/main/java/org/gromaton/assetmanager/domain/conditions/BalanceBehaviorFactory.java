package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.accounts.Account;

public interface BalanceBehaviorFactory {
    BalanceBehavior createFor(Account account);
}
