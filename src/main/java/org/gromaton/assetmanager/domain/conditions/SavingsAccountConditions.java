package org.gromaton.assetmanager.domain.conditions;

import org.gromaton.assetmanager.domain.recurrence.AbstractRecurrenceCondition;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.domain.temporal.DateTimePeriod;
import org.gromaton.assetmanager.javatimeextension.EndOfDay;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
public class SavingsAccountConditions extends AbstractConditions {
    private double interestRate;
    @OneToOne(targetEntity = AbstractRecurrenceCondition.class, cascade = CascadeType.ALL)
    private RecurrenceCondition payout;
    @Transient
    boolean isTopupable = true;

    public SavingsAccountConditions() {
    }

    public SavingsAccountConditions(SavingsAccountConditions conditions) {
        super(conditions);
        this.interestRate = conditions.interestRate;
        this.payout = conditions.payout;
    }

    public SavingsAccountConditions(String title, double interestRate, RecurrenceCondition payout) {
        this.setTitle(title);
        this.interestRate = interestRate;
        this.payout = payout;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public RecurrenceCondition getPayout() {
        return payout;
    }

    public void setPayout(RecurrenceCondition payout) {
        this.payout = payout;
    }

    public BigDecimal calcBalance(BalanceBehavior balanceBehavior, LocalDateTime start, LocalDateTime end) {
        //ToDo: Think about calculating only profit in this method, not the whole balance
        double dailyInterest = interestRate/365;
        DateTimePeriod calcPeriod = new DateTimePeriod(start, end);
        LocalDateTime nextPayout = payout.nextEventAfter(start);
        LocalDateTime current = start.with(new EndOfDay());
        BigDecimal currentProfit = BigDecimal.ZERO;
        BigDecimal payedOutProfit = BigDecimal.ZERO;
        BigDecimal balanceBaseForInterestCalc;
        while(!nextPayout.isAfter(end)) {
            //baseBalanceForInterestCalc = getMinBalanceForPeriod(balanceBehavior, current, nextPayout.minusDays(1).with(new EndOfDay()));
            while(current.isBefore(nextPayout)) {
                balanceBaseForInterestCalc = balanceBehavior.balanceOn(current).add(payedOutProfit);
                currentProfit = currentProfit.add(balanceBaseForInterestCalc.multiply(BigDecimal.valueOf(dailyInterest)));
                current = current.plusDays(1);
            }
            payedOutProfit = currentProfit;
            nextPayout = payout.nextEventAfter(nextPayout);
        }
        int scale = balanceBehavior.balanceOn(end).scale();
        return balanceBehavior.balanceOn(end).add(currentProfit).setScale(scale, BigDecimal.ROUND_HALF_DOWN);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Annual interest ").append(BigDecimal.valueOf(interestRate).movePointRight(2).toString()).append("%; ")
                .append(isTopupable ? "Top-ups are counted " : "Top-ups are not counted")
                .toString();
    }

    private BigDecimal getMinBalanceForPeriod(BalanceBehavior balanceBehavior, LocalDateTime start, LocalDateTime end) {
        BigDecimal currentMinimum = BigDecimal.valueOf(Double.MAX_VALUE);
        LocalDateTime timeStamp = LocalDateTime.of(start.toLocalDate(), LocalTime.MAX);
        while (!timeStamp.isAfter(end)) {
            currentMinimum = balanceBehavior.balanceOn(timeStamp).min(currentMinimum);
            timeStamp = timeStamp.plusDays(1);
        }
        return currentMinimum;
    }
}
