package org.gromaton.assetmanager.domain.conditions;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface BalanceBehavior {
    BigDecimal balanceOn(LocalDateTime timeStamp);
}
