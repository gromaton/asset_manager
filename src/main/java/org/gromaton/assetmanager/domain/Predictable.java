package org.gromaton.assetmanager.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface Predictable {
    //ToDo: chnage arg type to LocalDateTime
    public BigDecimal predict(LocalDate targetDate);
}
