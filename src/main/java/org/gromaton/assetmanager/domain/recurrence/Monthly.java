package org.gromaton.assetmanager.domain.recurrence;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

@Entity
public class Monthly extends AbstractRecurrenceCondition implements RecurrenceCondition {
    private static final int DEFAULT_DAY_IN_MONTH = 1;

    public static Monthly on(int day) {
        return new Monthly(day);
    }

    public static Monthly lastDay() {
        return new Monthly(31);
    }

    public static Monthly defaultDay() {
        return new Monthly(DEFAULT_DAY_IN_MONTH);
    }

    private int dayOfMonth;

    public Monthly() {
        this(DEFAULT_DAY_IN_MONTH);
    }

    private Monthly(int dayOfMonth) {
        if (dayOfMonth < 1 || dayOfMonth > 31) {
            throw new IllegalArgumentException("dayOfMonth argument should be within 1 and 31");
        }
        this.dayOfMonth = dayOfMonth;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    @Override
    public LocalDateTime nextEventAfter(LocalDateTime timeStamp) {
        int adjustedTimeStamp = Integer.min(dayOfMonth, timeStamp.toLocalDate().lengthOfMonth());
        LocalDateTime result;
        if (adjustedTimeStamp > timeStamp.getDayOfMonth()) {
            result = timeStamp.withDayOfMonth(adjustedTimeStamp);
        } else {
            adjustedTimeStamp = Integer.min(dayOfMonth, timeStamp.toLocalDate().plusMonths(1).lengthOfMonth());
            result = timeStamp.plusMonths(1).withDayOfMonth(adjustedTimeStamp);
        }
        return result;
    }

    @Override
    public Monthly copy() {
        return new Monthly(dayOfMonth);
    }

    @Override
    public String title() {
        return "Monthly on specific day of month";
    }
}
