package org.gromaton.assetmanager.domain.recurrence;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
public class Daily extends AbstractRecurrenceCondition implements RecurrenceCondition {
    @Override
    public LocalDateTime nextEventAfter(LocalDateTime timeStamp) {
        return timeStamp.plusDays(1);
    }

    @Override
    //ToDo: Why did I make this copy for immutable class?
    public RecurrenceCondition copy() {
        return new Daily();
    }

    @Override
    public String title() {
        return "Daily";
    }
}
