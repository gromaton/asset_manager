package org.gromaton.assetmanager.domain.recurrence;

import org.gromaton.assetmanager.domain.EntityWithIdLong;

import javax.persistence.Entity;

@Entity
public abstract class AbstractRecurrenceCondition extends EntityWithIdLong implements RecurrenceCondition {
}
