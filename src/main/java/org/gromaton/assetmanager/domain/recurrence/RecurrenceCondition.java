package org.gromaton.assetmanager.domain.recurrence;

import org.gromaton.assetmanager.domain.IEntity;

import java.time.LocalDateTime;

public interface RecurrenceCondition extends IEntity {
    LocalDateTime nextEventAfter(LocalDateTime timeStamp);
    RecurrenceCondition copy();
    default String title() {
        return this.getClass().getSimpleName();
    }
}
