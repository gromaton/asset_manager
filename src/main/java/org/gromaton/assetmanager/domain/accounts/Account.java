package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.IEntity;

import java.math.BigDecimal;

public interface Account extends IEntity {
    String getName();
    BigDecimal getBalance();
    String getSymbol();
    String typeTitle();
}
