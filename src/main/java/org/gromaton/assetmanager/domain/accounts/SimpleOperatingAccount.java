package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.operations.AbstractOperation;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.visitors.ViewOperations;
import org.gromaton.assetmanager.domain.temporal.DateTimePeriod;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
@Entity
public class SimpleOperatingAccount extends AbstractAccount implements OperatingAccount {
    @Embedded
    AssetType assetType = new AssetType();

    //ToDo: think about make orphanRemoval false and saving thus operation history (I need than FK in operations table)
    @OneToMany(targetEntity = AbstractOperation.class, cascade = { CascadeType.ALL }, orphanRemoval = true, fetch = FetchType.EAGER)
    List<Operation> operations = new ArrayList<>();
    @Transient
    ViewOperations viewOperations = new ViewOperations(operations);

    @PostLoad
    private void postLoad() {
        viewOperations = new ViewOperations(operations);
    }

    public SimpleOperatingAccount() {
        //viewOperations = new ViewOperations(operations);
    }

    public SimpleOperatingAccount(AssetType assetType) {
        this();
        this.assetType = assetType;
     }

    public void setAssetType(AssetType assetType) {
        this.assetType = assetType;
    }

    @Override
    public List<Operation> getOperations() {
        return viewOperations.getList(DateTimePeriod.untilNow());
    }

    @Override
    public List<Operation> getOperations(LocalDateTime start, LocalDateTime end) {
        return viewOperations.getList(new DateTimePeriod(start, end));
    }

    @Override
    public boolean add(Operation operation) throws Exception {
        if (!assetType.equals(operation.getAsset().type())) {
            throw new Exception("Adding operation with incompatible asset type is forbidden");
        }
        boolean result = false;
        if (!operations.contains(operation)) {
            result = operations.add(operation);
        }
        viewOperations.update();
        return result;
    }

    @Override
    public boolean remove(Operation operation) {
        boolean result = operations.remove(operation);
        viewOperations.update();
        return result;
    }

    @Override
    public BigDecimal getBalance(LocalDateTime toTimeStamp) {
        if (operations == null || operations.isEmpty()) {
            return BigDecimal.ZERO.setScale(getAssetType().scale());
        }
        return getOperationsSum(toTimeStamp).amount();
    }

    @Override
    public BigDecimal getBalance() {
        return getBalance(LocalDateTime.now());
    }

    private Asset getOperationsSum(LocalDateTime toTimeStamp) {
        BigDecimal sum = new ViewOperations(operations).getList(DateTimePeriod.allUpto(toTimeStamp))
                .stream()
                .map(op -> op.getAsset().amount())
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
        return new Asset(sum, assetType);
    }

    @Override
    public String getSymbol() {
        return assetType.symbol();
    }

    public AssetType getAssetType() {
        return assetType;
    }

    @Override
    public String typeTitle() {
        return "Operating Account";
    }
}