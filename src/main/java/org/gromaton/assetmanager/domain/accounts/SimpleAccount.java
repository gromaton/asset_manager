package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.math.BigDecimal;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Component
@Scope(SCOPE_PROTOTYPE)
@Entity
public class SimpleAccount extends AbstractAccount implements Account {
    @Embedded
    Asset asset = new Asset();

    public SimpleAccount() {
        super();
    }

    public SimpleAccount(String name, Asset asset) {
        super(name);
        setBalance(asset);
    }

    @Override
    public BigDecimal getBalance() {
        return asset.amount();
    }

    @Override
    public String getSymbol() {
        return asset.type().symbol();
    }

    public void setBalance(BigDecimal balance) {
        this.asset = new Asset(balance, asset.type());
    }

    public void setBalance(Asset asset) {
        this.asset = new Asset(asset.amount(), asset.type());
    }

    public AssetType getAssetType() {
        return asset.type();
    }

    public void setAssetType(AssetType assetType) {
        this.asset = new Asset(asset.amount(), assetType);
    }

    /*public void setSymbol(String symbol) {
        this.asset = new AssetImpl(getBalance(), new AssetType(symbol, asset.type().getScale(), asset.type().getMarketType()));
    }*/

    @Override
    public String typeTitle() {
        return "Simple Account";
    }
}