package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.EntityWithIdLong;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity(name = "accounts")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractAccount extends EntityWithIdLong implements Account {
    private String name = "Noname";

    public AbstractAccount() {
    }

    public AbstractAccount(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName() + ": " + getBalance() + " " + getSymbol();
    }
}

