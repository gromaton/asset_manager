package org.gromaton.assetmanager.domain.accounts;

import org.gromaton.assetmanager.domain.operations.Operation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface OperatingAccount extends Account {
    boolean add(Operation operation) throws Exception;
    boolean remove(Operation operation);
    BigDecimal getBalance(LocalDateTime toTimeStamp);
    List<Operation> getOperations();
    List<Operation> getOperations(LocalDateTime start, LocalDateTime end);
}