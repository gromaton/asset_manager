package org.gromaton.assetmanager.domain;

import java.math.BigDecimal;

public class BigDecimalUtils {
    public static BigDecimal trimZeroes(BigDecimal number, int minScale) {
        int scale = number.stripTrailingZeros().scale();
        return scale > minScale ? number.setScale(scale) : number.setScale(minScale);
    }
}
