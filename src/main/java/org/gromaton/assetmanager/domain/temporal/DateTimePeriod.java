package org.gromaton.assetmanager.domain.temporal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateTimePeriod {
    LocalDateTime start;
    LocalDateTime end;

    public static DateTimePeriod allUpto(LocalDateTime end) {
        return new DateTimePeriod(LocalDateTime.MIN, end);
    }

    public static DateTimePeriod untilNow() {
        return allUpto(LocalDateTime.now());
    }

    public DateTimePeriod(LocalDateTime start, LocalDateTime end) {
        this.start = start;
        this.end = end;
    }

    public DateTimePeriod(LocalDate start, LocalDate end) {
        this.start = LocalDateTime.of(start, LocalTime.MIN);
        this.end = LocalDateTime.of(end, LocalTime.MAX);
    }

    public boolean includes(LocalDateTime timeStamp) {
        return timeStamp != null && !timeStamp.isBefore(start) && !timeStamp.isAfter(end);
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }
}
