package org.gromaton.assetmanager.domain.assets;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

@Embeddable
public class AssetType {
    private final static String DEFAULT_SYMBOL = "EUR";
    private final static int DEFAULT_SCALE = 2;
    private final static Market DEFAULT_MARKET = Market.Money;

    private final String symbol;
    private final int scale;
    @Enumerated(value = EnumType.STRING)
    private final Market marketType;

    public AssetType() {
        this(DEFAULT_SYMBOL, DEFAULT_SCALE, DEFAULT_MARKET);
    }

    public AssetType(String symbol) {
        this(symbol, DEFAULT_SCALE, DEFAULT_MARKET);
    }

    public AssetType(String symbol, int scale, Market marketType) {
        this.symbol = symbol;
        this.scale = scale;
        this.marketType = marketType;
    }

    public String symbol() {
        return symbol;
    }

    public int scale() {
        return scale;
    }

    public Market marketType() {
        return marketType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssetType)) return false;
        AssetType assetType = (AssetType) o;
        return scale() == assetType.scale() &&
                Objects.equals(symbol(), assetType.symbol()) &&
                marketType() == assetType.marketType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol(), scale(), marketType());
    }
}
