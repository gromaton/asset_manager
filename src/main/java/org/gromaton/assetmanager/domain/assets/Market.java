package org.gromaton.assetmanager.domain.assets;

public enum Market {
    Money,
    CryptoCurrency,
    Stocks,
    Other
}
