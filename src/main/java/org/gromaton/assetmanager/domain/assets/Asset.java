package org.gromaton.assetmanager.domain.assets;

import org.gromaton.assetmanager.domain.Rounding;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.math.BigDecimal;
import java.util.Objects;

@Embeddable
public class Asset {
    private static final int STORED_SCALE = 19; //Assumed that all assets are stored with this scale(number of decimals)
    @Column(precision = 38, scale = STORED_SCALE)
    private final BigDecimal amount;
    @Embedded
    private final AssetType type;

    public Asset() {
        this(BigDecimal.ZERO, new AssetType());
    }

    public Asset(BigDecimal amount, AssetType type) {
        this.amount = amount;
        this.type = type;
    }

    public Asset(String amount, AssetType type) {
        this(new BigDecimal(amount), type);
    }

    public Asset(BigDecimal amount) {
        this(amount, new AssetType());
    }

    public Asset(String amount) {
        this(amount, new AssetType());
    }

    public BigDecimal amount() {
        return amount.setScale(type().scale(), Rounding.DEFAULT_MODE);
    }

    public AssetType type() {
        return type;
    }

    @Override
    public String toString() {
        return amount().toPlainString() + " " + type().symbol();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Asset)) return false;
        Asset asset = (Asset) o;
        return  Objects.equals(amount().stripTrailingZeros(), asset.amount().stripTrailingZeros()) &&
                Objects.equals(type(), asset.type());
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount(), type());
    }
}
