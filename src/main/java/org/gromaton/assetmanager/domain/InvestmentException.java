package org.gromaton.assetmanager.domain;

public class InvestmentException extends Exception {
    public InvestmentException() {
    }

    public InvestmentException(String message) {
        super(message);
    }

    public InvestmentException(String message, Throwable cause) {
        super(message, cause);
    }
}
