package org.gromaton.assetmanager.configuration.text;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "text")
public class MainLayoutTextConfig {
    @NotNull private String applicationName;
    @NotNull private String appNamePostfix;
    @NotNull private boolean demoComment;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getAppNamePostfix() {
        return appNamePostfix;
    }

    public void setAppNamePostfix(String appNamePostfix) {
        this.appNamePostfix = appNamePostfix;
    }

    public boolean isDemoComment() {
        return demoComment;
    }

    public void setDemoComment(boolean demoComment) {
        this.demoComment = demoComment;
    }
}
