package org.gromaton.assetmanager.db;

import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NewConditionsRepository extends CrudRepository<AbstractConditions, Long> {
    List<AbstractConditions> findAll();
}
