package org.gromaton.assetmanager.db;

import org.gromaton.assetmanager.domain.accounts.AbstractAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<AbstractAccount, Long> {
    List<AbstractAccount> findAll();
}
