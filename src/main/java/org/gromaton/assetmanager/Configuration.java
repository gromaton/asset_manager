package org.gromaton.assetmanager;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.springframework.context.annotation.Bean;

import java.util.Map;

@org.springframework.context.annotation.Configuration
public class Configuration {

   /* @Bean(name = "menuBar")
    public CrudMenuBar menuBar() {
        System.out.println(">>>>>>>Configurator is working...");
        return new CrudMenuBar();
    }*/
    @Bean
    public Map<Account, String> getAccountsMap(Map<String, Account> map) {
        BiMap<String, Account> tmp = HashBiMap.create();
        tmp.putAll(map);
        Map<Account, String> result = tmp.inverse();
        return result;
    }

    @Bean/*(name = "myCustomString")*/
    public String myCustomString() {
        return "myCustomString";
    }
}
