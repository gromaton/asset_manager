package org.gromaton.assetmanager.service;

import org.gromaton.assetmanager.db.NewConditionsRepository;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewConditionsService {
    NewConditionsRepository conditionsRepository;

    public NewConditionsService(NewConditionsRepository conditionsRepository) {
        this.conditionsRepository = conditionsRepository;
    }

    public List<AbstractConditions> findAll() {
        return conditionsRepository.findAll();
    }

    public AbstractConditions save(AbstractConditions conditions) {
        return conditionsRepository.save(conditions);
    }

    public void deleteAll(Iterable<AbstractConditions> conditions) {
        conditionsRepository.deleteAll(conditions);
    }
}
