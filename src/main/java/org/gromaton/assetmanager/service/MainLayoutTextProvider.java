package org.gromaton.assetmanager.service;

import org.gromaton.assetmanager.configuration.text.MainLayoutTextConfig;
import org.springframework.stereotype.Service;

@Service
public class MainLayoutTextProvider {
    private MainLayoutTextConfig textConfig;

    public MainLayoutTextProvider(MainLayoutTextConfig textConfig) {
        this.textConfig = textConfig;
    }

    public String applicationTitle() {
        return new StringBuilder(textConfig.getApplicationName())
                .append(" ")
                .append(textConfig.getAppNamePostfix())
                .toString();
    }

    public String homePageText() {
        StringBuilder mainText = new StringBuilder()
                .append("<p style=\"text-indent:20px;\">")
                .append("Asset Management System is an application to track your personal assets, ")
                .append("balances, operations and to analyse different investment offers ")
                .append("with a feature to evaluate the potential profit.</p>")
                .append("<p style=\"text-indent:20px;\">")
                .append("To manage your financial assets use the <a href=\"/accounts\">Accounts</a> page. ")
                .append("To observe available opportunities and calculate potential profit choose ")
                .append("the <a href=\"/opportunities\">Opportunities</a> page.</p>");
        StringBuilder demoComment = new StringBuilder()
                .append("<p style=\"color:#d96459\">")
                .append("This is a DEMO version, it is allowed to create, change and delete everything. Enjoy!!!")
                .append("</p>");
        if (textConfig.isDemoComment()) mainText.append(demoComment.toString());

        return mainText.insert(0, "<div style=\"padding:10px;\">").append("</div>").toString();
    }
}
