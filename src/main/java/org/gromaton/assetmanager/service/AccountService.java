package org.gromaton.assetmanager.service;

import org.gromaton.assetmanager.db.AccountRepository;
import org.gromaton.assetmanager.domain.accounts.AbstractAccount;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AccountService {
    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> findAll() {
        return accountRepository.findAll().stream().filter(e -> e instanceof Account).map(e -> (Account)e).collect(Collectors.toList());
    }

    public AbstractAccount save(Account account) {
        if (account instanceof AbstractAccount)
            return accountRepository.save((AbstractAccount)account);
        return null;
    }

    public void deleteAll(Set<Account> accounts) {
        accountRepository.deleteAll(accounts.stream()
                .filter(e -> e instanceof AbstractAccount)
                .map(e -> (AbstractAccount) e)
                .collect(Collectors.toSet())
        );
    }
}
