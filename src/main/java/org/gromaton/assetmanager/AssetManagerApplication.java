package org.gromaton.assetmanager;

import org.gromaton.assetmanager.domain.accounts.OperatingAccount;
import org.gromaton.assetmanager.domain.accounts.SimpleAccount;
import org.gromaton.assetmanager.domain.accounts.SimpleOperatingAccount;
import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.conditions.SavingsAccountConditions;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;
import org.gromaton.assetmanager.domain.recurrence.Monthly;
import org.gromaton.assetmanager.service.AccountService;
import org.gromaton.assetmanager.service.NewConditionsService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootApplication
public class AssetManagerApplication {
	static public ConfigurableApplicationContext appContext;

	public static void main(String[] args) {
		appContext = SpringApplication.run(AssetManagerApplication.class, args);
		//InsertInitialDataIntoDB();
		//String str = (String) appContext.getBean("myCustomString");
		//System.out.println(str);
	}

	public static void InsertInitialDataIntoDB() {
		AccountService accountService = (AccountService) appContext.getBean("accountService");
		accountService.save(new SimpleAccount("Cash", new Asset(new BigDecimal("250.00"), new AssetType())));
		accountService.save(new SimpleAccount("Bank", new Asset(new BigDecimal("1500.65"), new AssetType())));
		accountService.save(new SimpleAccount("Crypto", new Asset(new BigDecimal("0.01"), new AssetType("BTC", 8, Market.CryptoCurrency))));
		OperatingAccount eurOpAcc = new SimpleOperatingAccount(new AssetType());
		try {
			eurOpAcc.add(new SimpleOperation(new Asset("100"), LocalDateTime.now()));
			eurOpAcc.add(new SimpleOperation(new Asset("110"), LocalDateTime.now().minusDays(1)));

			eurOpAcc.add(new RecurringOperation(new SimpleOperation(new Asset("200"), LocalDateTime.now().minusMonths(2)), Monthly.lastDay()));
			eurOpAcc.add(new RecurringOperation(new SimpleOperation(new Asset("-15"), LocalDateTime.now().minusMonths(1)), Monthly.on(1)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		eurOpAcc.getOperations().get(0).setDescription("Test");
		accountService.save(eurOpAcc);

		NewConditionsService conditionsService = (NewConditionsService) appContext.getBean("newConditionsService");
		//RecurrenceCondition payOutCondition = Monthly.on(5);
		conditionsService.save(new SavingsAccountConditions("Some savings account", 0.015, Monthly.on(5)));
		conditionsService.save(new SavingsAccountConditions("Another savings account", 0.02, Monthly.on(5)));
		/*conditionsService.save(new SavingsAccountConditions.Builder("ComerzBank savings account")
				.setInterestRate(1.5)
				.setMonthlyTopUp(new BigDecimal("100.00"))
				.build()
		);*/
	}
}
