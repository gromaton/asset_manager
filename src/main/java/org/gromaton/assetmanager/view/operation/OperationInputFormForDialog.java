package org.gromaton.assetmanager.view.operation;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.recurrence.Daily;
import org.gromaton.assetmanager.domain.recurrence.Monthly;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.GenericInputFormForDialog;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringComponent
@Scope("prototype")
public class OperationInputFormForDialog extends GenericInputFormForDialog<Operation> {
    private final Select<Operation> typeSelect = new Select<>();
    private final Checkbox recurringBox = new Checkbox("Recurring Operation");

    private OperationFieldsAbstractFactory fieldsFactory;
    private List<Operation> implementedOperations;
    private AssetType defaultAssetType;

    public OperationInputFormForDialog(OperationFieldsAbstractFactory fieldsFactory, List<Operation> implementedOperations) {
        this.fieldsFactory = fieldsFactory;
        this.implementedOperations = implementedOperations;
        configureOperationTypeSelect();
        configureRecurrenceCheckBox();
        addComponentAsFirst(recurringBox);
        addComponentAsFirst(typeSelect);
    }

    public void setDefaultAssetType(AssetType defaultAssetType) {
        this.defaultAssetType = defaultAssetType;
        this.updateFields();
    }

    @Override
    public void read(Operation src) {
        //ToDo: refactor this to get rid of instanceof
        Class<? extends Operation> classForSelectField;
        Operation operationToRead;
        if (src instanceof RecurringOperation) {
            classForSelectField = ((RecurringOperation) src).getBaseOperation().getClass();
            recurringBox.setValue(true);
            operationToRead = src;
        } else if (src instanceof RecurringOperation.RecurringOperationDerivative) {
            operationToRead = ((RecurringOperation.RecurringOperationDerivative) src).getOrigin();
            classForSelectField = ((RecurringOperation) operationToRead).getBaseOperation().getClass();
            recurringBox.setValue(true);
        } else {
            classForSelectField = src.getClass();
            recurringBox.setValue(false);
            operationToRead = src;
        }
        typeSelect.setValue(implementedOperations.stream().filter(e -> e.getClass() == classForSelectField).findFirst().get());
        typeSelect.setEnabled(false);
        recurringBox.setEnabled(false);
        super.read(operationToRead);
    }

    @Override
    protected GenericEntityFields<Operation> retrieveInputFields() {
        Operation defaultOperation = typeSelect.getValue();
        if (recurringBox.getValue()) {
            defaultOperation = new RecurringOperation(defaultOperation, /*Monthly.defaultDay()*/new Daily());
        }
        GenericEntityFields<Operation> inputFields = (GenericEntityFields<Operation>) fieldsFactory.create(defaultOperation);
        if (defaultAssetType != null) {
            defaultOperation.setAssetType(defaultAssetType);
            inputFields.setDefaultValuesSource(defaultOperation);
        }
        return inputFields;
    }

    @Override
    public void reset() {
        super.reset();
        typeSelect.setEnabled(true);
        recurringBox.setEnabled(true);
    }

    private void configureOperationTypeSelect() {
        typeSelect.setLabel("Operation Type");
        typeSelect.setItems(implementedOperations);
        typeSelect.setItemLabelGenerator(Operation::typeTitle);
        typeSelect.addValueChangeListener(event -> updateFields());
        typeSelect.setValue(implementedOperations.get(0));
        typeSelect.setWidthFull();
    }

    private void configureRecurrenceCheckBox() {
        recurringBox.addValueChangeListener(event -> updateFields());
    }
}
