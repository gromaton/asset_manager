package org.gromaton.assetmanager.view.operation;

import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.NotSupportedEntityFields;
import org.springframework.context.annotation.Scope;

import java.util.List;

//The factory is not in use anymore
//@SpringComponent
@Scope("prototype")
public class OperationFieldsFactoryImpl implements OperationFieldsAbstractFactory {
    List<GenericEntityFields<? extends Operation>> availableFieldImpls;

    public OperationFieldsFactoryImpl(List<GenericEntityFields<? extends Operation>> availableFieldImpls) {
        System.out.println(">>> OperationFieldsFactoryImpl object is being created...");
        this.availableFieldImpls = availableFieldImpls;
    }

    @Override
    public GenericEntityFields<? extends Operation> create(Operation operation) {
        return availableFieldImpls.stream()
                .filter(fields -> fields.getEntityClass().equals(operation.getClass()))
                .findFirst()
                .orElse(new NotSupportedEntityFields("Error: This operation type is not supported"));
    }
}
