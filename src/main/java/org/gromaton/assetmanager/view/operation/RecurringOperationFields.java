package org.gromaton.assetmanager.view.operation;

import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.recurrence.RecurrenceFields;
import org.gromaton.assetmanager.view.recurrence.RecurrenceFieldsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope("prototype")
public class RecurringOperationFields extends GenericEntityFields<RecurringOperation> {
    private RecurringOperation formBean;
    private GenericEntityFields<Operation> baseOperationFields;
    private RecurrenceFields recurrenceFields;
    private RecurrenceFieldsFactory recurrenceFieldsFactory;

    public RecurringOperationFields(RecurrenceFieldsFactory recurrenceFieldsFactory) {
        this.recurrenceFields = new RecurrenceFields(recurrenceFieldsFactory);
        add(recurrenceFields);
    }

    public void setBaseOperationFields(GenericEntityFields<? extends Operation> baseOperationFields) {
        if (this.baseOperationFields != null) {
            remove(this.baseOperationFields);
        }
        this.baseOperationFields = (GenericEntityFields<Operation>) baseOperationFields;
        if (baseOperationFields != null) {
            addComponentAsFirst(baseOperationFields);
        }
    }

    @Override
    public RecurringOperation getEntity() {
        if (formBean == null) {
            formBean = new RecurringOperation(baseOperationFields.getEntity(), recurrenceFields.getEntity());
        } else {
            formBean.setBaseOperation(baseOperationFields.getEntity());
            formBean.setRecurrenceCondition(recurrenceFields.getEntity());
        }
        return formBean;
    }

    @Override
    public void setEntity(RecurringOperation entity) {
        if (entity == null) throw new IllegalArgumentException("Cannot set null in RecurringOperation input form");
        checkOperationAndFieldsCompatibility(entity);
        formBean = entity;
        baseOperationFields.setEntity(entity.getBaseOperation());
        recurrenceFields.setEntity(entity.getRecurrenceCondition());
    }

    @Override
    public void setDefaultValuesSource(RecurringOperation source) {
        baseOperationFields.setDefaultValuesSource(source.getBaseOperation());
        recurrenceFields.setDefaultValuesSource(source.getRecurrenceCondition());
    }

    @Override
    public Class<RecurringOperation> getEntityClass() {
        return RecurringOperation.class;
    }

    @Override
    public void reset() {
        baseOperationFields.reset();
        recurrenceFields.reset();
        formBean = null;
    }

    private void checkOperationAndFieldsCompatibility(RecurringOperation operation) {
        if (baseOperationFields.getEntityClass() != operation.getBaseOperation().getClass()) {
            throw new IllegalArgumentException("Trying to set entity incompatible to fields");
        }
    }
}
