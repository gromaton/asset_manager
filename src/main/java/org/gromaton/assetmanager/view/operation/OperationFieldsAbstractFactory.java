package org.gromaton.assetmanager.view.operation;

import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.view.GenericEntityFields;

public interface OperationFieldsAbstractFactory {
    GenericEntityFields<? extends Operation> create(Operation operation);
}
