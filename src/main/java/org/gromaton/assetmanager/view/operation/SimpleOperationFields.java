package org.gromaton.assetmanager.view.operation;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.springframework.context.annotation.Scope;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Locale;

@SpringComponent
@Scope("prototype")
public class SimpleOperationFields extends GenericEntityFields<SimpleOperation> {
    DatePicker dateField = new DatePicker(LocalDate.now());
    BigDecimalField amountField = new BigDecimalField();
    TextField descriptionField = new TextField();
    Label labelSuffix = new Label();

    private SimpleOperation simpleOperation;
    private SimpleOperation defaultOperation = new SimpleOperation();
    private Binder<SimpleOperation> binder = new Binder<>(SimpleOperation.class);

    public SimpleOperationFields() {
        configureDateField();
        configureAmountField();
        configureDescriptionField();
        configureBinder();
        configureLayout();
        setFieldDefaults();
    }

    @Override
    public SimpleOperation getEntity() {
        try {
            binder.writeBean(getSimpleOperation());
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        return getSimpleOperation();
    }

    @Override
    public void setEntity(SimpleOperation entity) {
        this.simpleOperation = entity;
        binder.readBean(entity);
        labelSuffix.setText(getSimpleOperation().getAsset().type().symbol());
        amountField.setValue(getSimpleOperation().getAsset().amount());
    }

    @Override
    public void setDefaultValuesSource(SimpleOperation source) {
        if (source == null)
            defaultOperation = new SimpleOperation();
        else
            this.defaultOperation = source;
    }

    @Override
    public Class<SimpleOperation> getEntityClass() {
        return SimpleOperation.class;
    }

    @Override
    public void reset() {
        setFieldDefaults();
        simpleOperation = createNewOperationBasedOnDefaultValuesSource();
        labelSuffix.setText(simpleOperation.getAsset().type().symbol());
    }

    private void configureLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);
        dateField.setWidthFull();
        amountField.setWidthFull();
        descriptionField.setWidthFull();
        layout.add(dateField, amountField, descriptionField);
        add(layout);
    }

    private void configureDateField() {
        dateField.setLabel("Date");
        dateField.setLocale(Locale.GERMANY);
    }

    private void configureAmountField() {
        amountField.setLabel("Amount");
        amountField.setPlaceholder(BigDecimal.ZERO.setScale(getSimpleOperation().getAsset().type().scale()).toPlainString());
        amountField.setSuffixComponent(labelSuffix);
        amountField.addValueChangeListener(event -> labelSuffix.setText(getSimpleOperation().getAsset().type().symbol()));
    }

    private void configureDescriptionField() {
        descriptionField.setLabel("Description");
        descriptionField.setPlaceholder("Description");
    }

    private void setFieldDefaults() {
        binder.readBean(defaultOperation);
        labelSuffix.setText(defaultOperation.getAsset().type().symbol());
    }

    private void configureBinder() {
        binder.forField(dateField).bind(
                Operation::getDate,
                (operation, date) -> operation.setDate(date)
        );
        binder.forField(amountField).bind(
                operation -> operation.getAsset().amount(),
                (operation, amount) -> operation.setAmount(amount)
        );
        binder.forField(descriptionField).bind(
                Operation::getDescription,
                (operation, description) -> operation.setDescription(description)
        );
        binder.addValueChangeListener(event -> System.out.println(">>> " + this.getClass().getSimpleName() + ": some field value has been changed"));
        binder.addStatusChangeListener(event -> System.out.println(">>> " + this.getClass().getSimpleName() + ": binder status has been changed"));
    }

    private SimpleOperation getSimpleOperation() {
        if (simpleOperation == null) {
            simpleOperation = createNewOperationBasedOnDefaultValuesSource();
        }
        return simpleOperation;
    }

    private SimpleOperation createNewOperationBasedOnDefaultValuesSource() {
        return new SimpleOperation(defaultOperation.getAsset(), defaultOperation.getTimeStamp(), defaultOperation.getDescription());
    }
}
