package org.gromaton.assetmanager.view.operation;

import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.view.GenericDialogControlPanel;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope("prototype")
public class DialogOperationControlPanel extends GenericDialogControlPanel<Operation> {
    OperationInputFormForDialog inputFormForDialog;

    public DialogOperationControlPanel(OperationInputFormForDialog inputFormForDialog) {
        super(inputFormForDialog);
        System.out.println(">>> DialogOperationControlPanel object is being created...");
        this.inputFormForDialog = inputFormForDialog;
    }

    public void setDefaultAssetType(AssetType assetType) {
        inputFormForDialog.setDefaultAssetType(assetType);
    }
}
