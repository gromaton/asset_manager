package org.gromaton.assetmanager.view.operation;

import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.domain.operations.SimpleOperation;
import org.gromaton.assetmanager.domain.operations.visitors.OperationVisitor;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OperationInputFieldsFactory implements OperationVisitor, OperationFieldsAbstractFactory {
    private GenericEntityFields<? extends Operation> inputFields;
    private final ConfigurableApplicationContext appContext;

    //ToDo: to get rid of the whole context dependency
    public OperationInputFieldsFactory(ConfigurableApplicationContext appContext) {
        this.appContext = appContext;
    }

    @Override
    public GenericEntityFields<? extends Operation> create(Operation operation) {
        operation.accept(this);
        return inputFields;
    }

    @Override
    public void visit(SimpleOperation operation) {
        inputFields = appContext.getBean(SimpleOperationFields.class);
    }

    @Override
    public void visit(RecurringOperation operation) {
        RecurringOperationFields recurringOperationFields = appContext.getBean(RecurringOperationFields.class);
        recurringOperationFields.setBaseOperationFields(this.create(operation.getBaseOperation()));
        inputFields = recurringOperationFields;
    }

    @Override
    public void visit(RecurringOperation.RecurringOperationDerivative operation) {
        //ToDo: Implement this to have some functionality related with Derivative in the InputForm
    }
}
