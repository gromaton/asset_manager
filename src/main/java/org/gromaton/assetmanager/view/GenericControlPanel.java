package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.*;
import com.vaadin.flow.shared.Registration;
import org.gromaton.assetmanager.domain.IEntity;

@Tag("div")
public class GenericControlPanel<T extends IEntity> extends Component implements HasComponents {
    private CrudMenuBar crudMenuBar = new CrudMenuBar();
    private GenericInputForm<T> inputForm;

    public GenericControlPanel(GenericInputForm<T> inputForm) {
        this.inputForm = inputForm;
        crudMenuBar.getRefreshItem().addClickListener(event -> fireEvent(new RefreshEvent(this)));
        crudMenuBar.getDeleteItem().addClickListener(event -> fireEvent(new DeleteEvent(this)));
        crudMenuBar.getEditItem().addClickListener(event -> fireEvent(new EditEvent(this)));
        inputForm.addSaveListener(event -> fireEvent(new SaveEvent(this, event.getEntity())));
        crudMenuBar.getCreateItem().addClickListener(event -> fireEvent(new CreateEvent(this)));
        add(crudMenuBar);
    }

    public CrudMenuBar getMenuBar() {
        return crudMenuBar;
    }

    public void addInputFormToComponent(HasComponents dstComponent) {
        dstComponent.add(inputForm);
    }

    public void read(T entity) {
        inputForm.read(entity);
    }

    public void reset() {
        inputForm.reset();
    }

    public Registration addRefreshClickListener(ComponentEventListener<GenericControlPanel<T>.RefreshEvent> listener) {
        return addListener(getRefreshEventClass(), listener);
    }

    public Registration addDeleteClickListener(ComponentEventListener<DeleteEvent> listener) {
        return addListener(getDeleteEventClass(), listener);
    }

    public Registration addCreateClickListener(ComponentEventListener<CreateEvent> listener) {
        return addListener(getCreateEventClass(), listener);
    }

    public Registration addEditClickListener(ComponentEventListener<EditEvent> listener) {
        return addListener(getEditEventClass(), listener);
    }

    public Registration addSaveListener(ComponentEventListener<SaveEvent> listener) {
        return addListener(getSaveEventClass(), listener);
    }

    private Class getRefreshEventClass() {
        return RefreshEvent.class;
    }

    private Class getDeleteEventClass() {
        return DeleteEvent.class;
    }

    private Class getCreateEventClass() {
        return CreateEvent.class;
    }

    private Class getEditEventClass() {
        return EditEvent.class;
    }

    private Class getSaveEventClass() {
        return SaveEvent.class;
    }

    protected abstract class ControlPanelEvent extends ComponentEvent<GenericControlPanel> {
        public ControlPanelEvent(GenericControlPanel source, boolean fromClient) {
            super(source, fromClient);
        }
    }

    public class RefreshEvent extends ControlPanelEvent {
        public RefreshEvent(GenericControlPanel source) {
            super(source, false);
        }
    }

    public class DeleteEvent extends ControlPanelEvent {
        public DeleteEvent(GenericControlPanel source) {
            super(source, false);
        }
    }

    public class CreateEvent extends ControlPanelEvent {
        public CreateEvent(GenericControlPanel source) {
            super(source, false);
        }
    }

    public class EditEvent extends ControlPanelEvent {
        public EditEvent(GenericControlPanel source) {
            super(source, false);
        }
    }

    public class SaveEvent extends ControlPanelEvent {
        T entity;
        public SaveEvent(GenericControlPanel source, T entity) {
            super(source, false);
            this.entity = entity;
        }

        public T getEntity() {
            return entity;
        }
    }
}
