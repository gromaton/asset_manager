package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.gromaton.assetmanager.domain.IEntity;

public abstract class GenericInputFormForDialog<T extends IEntity> extends GenericInputForm<T> {
    private final Button saveButton = new Button("Save");
    private final Button closeButton = new Button("Close");

    public GenericInputFormForDialog() {
        HorizontalLayout buttonsLayout = new HorizontalLayout(saveButton, closeButton);
        buttonsLayout.setWidthFull();
        buttonsLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        add(buttonsLayout);
        closeButton.addClickListener(click -> fireEvent(new GenericInputForm.CloseEvent(this)));
        saveButton.addClickListener(click -> fireEvent(new GenericInputForm.SaveEvent(this)));
    }
}
