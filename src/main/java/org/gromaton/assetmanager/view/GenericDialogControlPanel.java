package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.dialog.Dialog;
import org.gromaton.assetmanager.domain.IEntity;

public class GenericDialogControlPanel<T extends IEntity> extends GenericControlPanel<T> {
    private Dialog dialogWindow = new Dialog();

    public GenericDialogControlPanel(GenericInputFormForDialog<T> inputFormForDialog) {
        super(inputFormForDialog);
        addInputFormToComponent(dialogWindow);
        configureInputFormListeners(inputFormForDialog);
    }

    public void openDialog() {
        dialogWindow.open();
    }

    public void closeDialog() {
        dialogWindow.close();
    }

    @Override
    public void read(T entity) {
        super.read(entity);
    }

    private void configureInputFormListeners(GenericInputFormForDialog<T> inputFormForDialog) {
        inputFormForDialog.addCloseListener(event -> dialogWindow.close());
        inputFormForDialog.addSaveListener(event -> dialogWindow.close());
    }
}
