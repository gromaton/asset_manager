package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.gromaton.assetmanager.service.MainLayoutTextProvider;

@Route(value = "", layout = MainLayout.class)
@PageTitle("Home | AMS")
public class HomePage extends VerticalLayout {
    public HomePage(MainLayoutTextProvider textProvider) {
        //String str = "<div style=\"padding:10px;\"><p style=\"text-indent:20px;\">Asset Management System is an application to track your personal assets, balances, operations and to analyse different investment offers with a feature to evaluate the potential profit.</p><p style=\"text-indent:20px;\">To manage your financial assets use the <a href=\"/accounts\">Accounts</a> page. To observe available opportunities and calculate potential profit choose the <a href=\"/opportunities\">Opportunities</a> page.</p></div>";
        Html htmlText = new Html(textProvider.homePageText());
        Span span = new Span(htmlText);
        span.setWidth("450px");
        add(span);
    }
}
