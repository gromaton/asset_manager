package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.html.Label;
import org.gromaton.assetmanager.domain.IEntity;

public class NotSupportedEntityFields extends GenericEntityFields {
    Label label;

    public NotSupportedEntityFields(String errorMsg) {
        this.label = new Label(errorMsg);
        this.label.getStyle().set("color", "red");
        add(label);
    }

    @Override
    public IEntity getEntity() {
        return null;
    }

    @Override
    public void setEntity(IEntity entity) {

    }

    @Override
    public void setDefaultValuesSource(IEntity source) {

    }

    @Override
    public Class getEntityClass() {
        return null;
    }

    @Override
    public void reset() {

    }
}
