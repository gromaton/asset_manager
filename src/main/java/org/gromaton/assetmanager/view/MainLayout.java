package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.RouterLink;
import org.gromaton.assetmanager.service.MainLayoutTextProvider;

@CssImport("./css/styles.css")
public class MainLayout extends AppLayout {
    private final MainLayoutTextProvider textProvider;

    public MainLayout(MainLayoutTextProvider textProvider) {
        this.textProvider = textProvider;
        createHeader();
        createDrawer();
        setPrimarySection(AppLayout.Section.NAVBAR);
    }

    private void createHeader() {
        Span caption = new Span(textProvider.applicationTitle());
        caption.getStyle().set("font-weight", "bold");
        HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), caption);
        header.setWidth("100%");
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        addToNavbar(true, header);
    }

    private void createDrawer() {
        RouterLink homePageLink = new RouterLink("Home", HomePage.class);
        homePageLink.setHighlightCondition(HighlightConditions.sameLocation());
        RouterLink accountPageLink = new RouterLink("Accounts", AccountPage.class);
        RouterLink opportunitiesPageLink = new RouterLink("Opportunities", OpportunitiesPage.class);
        VerticalLayout drawerLayout = new VerticalLayout(
                homePageLink,
                accountPageLink,
                opportunitiesPageLink
        );
        addToDrawer(drawerLayout);

    }
}
