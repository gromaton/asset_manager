package org.gromaton.assetmanager.view.account;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.view.GenericEntityFields;

public interface AccountFieldsAbstractFactory {
    GenericEntityFields<? extends Account> create(Account account);
}
