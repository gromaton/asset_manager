package org.gromaton.assetmanager.view.account;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.accounts.SimpleOperatingAccount;
import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.operations.Operation;
import org.gromaton.assetmanager.domain.operations.RecurringOperation;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.operation.DialogOperationControlPanel;
import org.springframework.context.annotation.Scope;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.vaadin.flow.component.grid.GridVariant.LUMO_COMPACT;
import static com.vaadin.flow.component.grid.GridVariant.LUMO_NO_BORDER;

@SpringComponent
@Scope("prototype")
public class SimpleOperatingAccountFields extends GenericEntityFields<SimpleOperatingAccount> {
    TextField name = new TextField("Account name");
    AssetTypeField assetType = new AssetTypeField();
    DatePicker fromDate;
    DatePicker toDate;
    Grid<Operation> operationGrid = new Grid<>(Operation.class, false);

    private SimpleOperatingAccount inputAccount;
    private SimpleOperatingAccount defaultAccount = new SimpleOperatingAccount();
    private Binder<SimpleOperatingAccount> binder = new Binder<>(SimpleOperatingAccount.class);
    private DialogOperationControlPanel operationControlPanel;

    public SimpleOperatingAccountFields(DialogOperationControlPanel operationControlPanel) {
        this.operationControlPanel = operationControlPanel;
        configureMarketSelectFieldInAssetType();
        configureScaleFieldInAssetType();
        configureOperationGrid();
        configureOperationControlPanel();

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setPadding(false);
        mainLayout.setSpacing(false);
        name.setWidthFull();
        assetType.setWidthFull();
        mainLayout.add(name, assetType, createAndConfigureOperationControls(), operationGrid);
        //layout.add(new Button("Temp", click -> { }));
        setFieldDefaults();
        add(mainLayout);
        binder.bindInstanceFields(this);
    }

    @Override
    public SimpleOperatingAccount getEntity() {
        try {
            binder.writeBean(getInputAccount());
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        return inputAccount;
    }

    @Override
    public void setEntity(SimpleOperatingAccount entity) {
        /*if (entity != null) {
            this.inputAccount = entity;
            LocalDate startDate = inputAccount.getOperations().stream().map(e -> e.getDate()).min(LocalDate::compareTo).orElseGet(() -> LocalDate.now().minusMonths(1));
            fromDate.setValue(startDate);
        } else {
            this.inputAccount = createNewOperatingAccountImplBasedOnDefaultValuesSource();
        }*/
        Optional<SimpleOperatingAccount> optEntity = Optional.ofNullable(entity);
        this.inputAccount = optEntity.orElseGet(() -> newOperatingAccountImplBasedOnDefaultValuesSource());
        LocalDate startDate = inputAccount.getOperations().stream().map(e -> e.getDate()).min(LocalDate::compareTo).orElseGet(() -> LocalDate.now().minusMonths(1));
        fromDate.setValue(startDate);

        binder.readBean(inputAccount);
        assetType.setEnabled(false);
        setOperationGridItems(inputAccount.getOperations(LocalDateTime.of(fromDate.getValue(), LocalTime.MIN), LocalDateTime.of(toDate.getValue(), LocalTime.MAX)));
    }

    @Override
    public void setDefaultValuesSource(SimpleOperatingAccount source) {
        Optional<SimpleOperatingAccount> optSource = Optional.ofNullable(source);
        defaultAccount = optSource.orElseGet(() -> new SimpleOperatingAccount());
    }

    @Override
    public Class<SimpleOperatingAccount> getEntityClass() {
        return SimpleOperatingAccount.class;
    }

    @Override
    public void reset() {
        assetType.setEnabled(true);
        setFieldDefaults();
        inputAccount = newOperatingAccountImplBasedOnDefaultValuesSource();
        setOperationGridItems(inputAccount.getOperations(LocalDateTime.of(fromDate.getValue(), LocalTime.MIN), LocalDateTime.of(toDate.getValue(), LocalTime.MAX)));

    }

    private HorizontalLayout createAndConfigureOperationControls() {
        HorizontalLayout hlOperationControl = new HorizontalLayout();
        hlOperationControl.getStyle().set("margin-top", "10px");
        hlOperationControl.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.END);
        configureDateIntervalForm();
        hlOperationControl.add(operationControlPanel, fromDate, toDate);
        return hlOperationControl;
    }

    private void configureDateIntervalForm() {
        fromDate = new DatePicker(LocalDate.now().minusMonths(1));
        fromDate.setLabel("from");
        fromDate.setLocale(Locale.GERMANY);
        fromDate.setWeekNumbersVisible(true);
        toDate = new DatePicker(LocalDate.now());
        toDate.setLabel("to");
        toDate.setLocale(Locale.GERMANY);
        toDate.setWeekNumbersVisible(true);
        fromDate.addValueChangeListener(event -> {
            updateOperationGridItems();
        });
        toDate.addValueChangeListener(event -> updateOperationGridItems());
    }

    private void updateOperationGridItems() {
        setOperationGridItems(inputAccount.getOperations(LocalDateTime.of(fromDate.getValue(), LocalTime.MIN), LocalDateTime.of(toDate.getValue(), LocalTime.MAX)));
        refreshOperationGrid();
    }

    private void configureMarketSelectFieldInAssetType() {
        assetType.getMarketSelectField().setLabel("Market");
        assetType.getMarketSelectField().setItems(Market.values());
        assetType.getMarketSelectField().addValueChangeListener(event -> {
            updateScaleFieldEnabled();
        });
    }

    private void configureScaleFieldInAssetType() {
        assetType.getScaleField().setWidth("30%");
        assetType.getScaleField().setHasControls(true);
        assetType.getScaleField().setMin(0);
        assetType.getScaleField().setMax(19);
        assetType.getScaleField().addValueChangeListener(event -> {
            if (assetType.getScaleField().getValue() == null || assetType.getScaleField().getValue() < 0)
                assetType.getScaleField().setValue(0);
            if (assetType.getScaleField().getValue() > 19) assetType.getScaleField().setValue(19);
        });
        assetType.getScaleField().setEnabled(false);
    }

    private void configureOperationGrid() {
        operationGrid.addThemeVariants(LUMO_NO_BORDER, LUMO_COMPACT);
        operationGrid.setVerticalScrollingEnabled(true);
        DateTimeFormatter timeStampFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        operationGrid.addColumn(e -> e.getDate().format(timeStampFormatter))
                .setHeader("Date")
                .setKey("Date")
                .setFooter("Total Sum:");
        operationGrid.addColumn(e -> e.getAsset().amount().toPlainString() + " " + e.getAsset().type().symbol())
                .setHeader("Amount")
                .setKey("Amount");
        operationGrid.addColumn(e -> (e instanceof RecurringOperation || e instanceof RecurringOperation.RecurringOperationDerivative) ? "yes" : "no")
                .setHeader("Recurring")
                .setKey("Recurring");
        operationGrid.addColumn(Operation::getDescription)
                .setHeader("Description")
                .setKey("Description")
                .setFooter("Current Balance: " + getInputAccount().getBalance());
        operationGrid.setMaxHeight("250px");
    }

    private void configureOperationControlPanel() {
        //operationControlPanel.getMenuBar().getStyle().set("margin-top", "20px");
        configureOperationControlPanelListeners();
    }

    private void configureOperationControlPanelListeners() {
        operationControlPanel.addCreateClickListener(click -> {
            operationControlPanel.setDefaultAssetType(this.getEntity().getAssetType());
            operationControlPanel.reset();
            operationControlPanel.openDialog();
        });
        operationControlPanel.addSaveListener(event -> {
            try {
                getInputAccount().add(event.getEntity());
            } catch (Exception e) {
                e.printStackTrace();
            }
            refreshOperationGrid();
        });
        operationControlPanel.addEditClickListener(event -> {
            Operation selectedOperation = operationGrid.getSelectedItems().iterator().next();
            if (selectedOperation != null) {
                operationControlPanel.read(selectedOperation);
                operationControlPanel.openDialog();
            }
        });
        operationControlPanel.addDeleteClickListener(event -> {
            for (Operation operation : operationGrid.getSelectedItems()) {
                boolean isRemoved = getInputAccount().remove(operation);
                if (!isRemoved) {
                    Label text = new Label("This operation cannot be removed");
                    Dialog notRemovedInfo = new Dialog(text);
                    notRemovedInfo.open();
                }
            }
            refreshOperationGrid();
        });
        operationControlPanel.addRefreshClickListener(event -> updateOperationGridItems());
    }

    private void setOperationGridItems(Collection<Operation> operations) {
        operationGrid.setItems(operations);
        updateTotalOperationSum();
        updateAccountBalance();
    }

    private void updateTotalOperationSum() {
        FooterRow footerRow = operationGrid.getFooterRows().get(0);
        FooterRow.FooterCell totalAmountCell = footerRow.getCell(operationGrid.getColumnByKey("Amount"));
        totalAmountCell.setText(calcDisplayedOperationSum().toString());
    }

    private void updateAccountBalance() {
        FooterRow footerRow = operationGrid.getFooterRows().get(0);
        footerRow.getCell(operationGrid.getColumnByKey("Description"))
                .setText("Current Balance: " + getInputAccount().getBalance());
    }

    private Asset calcDisplayedOperationSum() {
        List<Operation> gridOperations = operationGrid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList());
        if (gridOperations == null || gridOperations.isEmpty()) {
            return new Asset(BigDecimal.ZERO, new AssetType(""));
        }
        return new Asset(gridOperations.stream().map(op -> op.getAsset().amount()).reduce(BigDecimal::add).get(), getInputAccount().getAssetType());
    }

    private void refreshOperationGrid() {
        operationGrid.getDataProvider().refreshAll();
        updateTotalOperationSum();
        updateAccountBalance();
    }

    private void setFieldDefaults() {
        name.clear();
        name.setPlaceholder("e.g. Cash");
        int defaultScale = 2;
        assetType.getScaleField().setValue(defaultScale);
        assetType.getMarketSelectField().setValue(Market.Money);
        updateScaleFieldEnabled();
        assetType.getSymbolField().setPlaceholder("e.g. EUR");
        assetType.getSymbolField().setValue("EUR");
        fromDate.setValue(LocalDate.now().minusMonths(1));
        toDate.setValue(LocalDate.now());
    }

    private void updateScaleFieldEnabled() {
        assetType.getScaleField().setEnabled(assetType.getMarketSelectField().getValue() != Market.Money);
    }

    private SimpleOperatingAccount getInputAccount() {
        if (inputAccount == null) {
            inputAccount = newOperatingAccountImplBasedOnDefaultValuesSource();
        }
        return inputAccount;
    }

    private SimpleOperatingAccount newOperatingAccountImplBasedOnDefaultValuesSource() {
        return new SimpleOperatingAccount(defaultAccount.getAssetType());
    }
}
