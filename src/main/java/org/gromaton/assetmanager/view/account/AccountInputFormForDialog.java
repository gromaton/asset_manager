package org.gromaton.assetmanager.view.account;

import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.GenericInputFormForDialog;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringComponent
@Scope("prototype")
public class AccountInputFormForDialog extends GenericInputFormForDialog<Account> {
    private final Select<Account> typeSelect = new Select();

    private AccountFieldsAbstractFactory factory;
    private List<Account> implementedAccounts;

    public AccountInputFormForDialog(AccountFieldsAbstractFactory factory, List<Account> implementedAccounts) {
        System.out.println(">>> AccountInputFormForDialog is being created ...");
        this.factory = factory;
        this.implementedAccounts = implementedAccounts;
        configureTypeSelect();
        addComponentAsFirst(typeSelect);
    }

    @Override
    public void read(Account src) {
        typeSelect.setValue(implementedAccounts.stream().filter(e -> e.getClass() == src.getClass()).findFirst().get());
        typeSelect.setEnabled(false);
        super.read(src);
    }

    @Override
    protected GenericEntityFields<Account> retrieveInputFields() {
        return (GenericEntityFields<Account>) factory.create(typeSelect.getValue());
    }

    @Override
    public void reset() {
        super.reset();
        typeSelect.setEnabled(true);
    }

    private void configureTypeSelect() {
        typeSelect.setLabel("Account Type");
        typeSelect.setItems(implementedAccounts);
        typeSelect.setItemLabelGenerator(Account::typeTitle);
        typeSelect.addValueChangeListener(event -> {
            updateFields();
            super.reset();
        });
        typeSelect.setValue(implementedAccounts.get(0));
        typeSelect.setWidthFull();
    }
}