package org.gromaton.assetmanager.view.account;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.assets.Asset;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;
import org.gromaton.assetmanager.domain.accounts.SimpleAccount;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.springframework.context.annotation.Scope;

import java.math.BigDecimal;
import java.math.RoundingMode;

@SpringComponent
@Scope("prototype")
public class SimpleAccountFields extends GenericEntityFields<SimpleAccount> {
    TextField name = new TextField("Account name");
    BigDecimalField balance = new BigDecimalField("Balance");
    Select<Market> marketSelect = new Select<>();
    TextField symbol = new TextField("Symbol");
    IntegerField scale = new IntegerField("Scale");

    private SimpleAccount simpleAccount;
    private SimpleAccount defaultAccount = new SimpleAccount();
    private Binder<SimpleAccount> binder = new Binder<>(SimpleAccount.class);

    public SimpleAccountFields() {
        System.out.println(">>> " + this.getClass().getSimpleName() + " object is being created...");
        configureMarketSelect();
        configureBalanceField();
        configureScaleField();
        setFieldDefaults();
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);
        name.setWidthFull();
        HorizontalLayout hlBalanceSymbol = new HorizontalLayout();
        hlBalanceSymbol.setWidthFull();
        hlBalanceSymbol.expand(balance);
        symbol.setWidth("30%");
        hlBalanceSymbol.add(balance, symbol);
        HorizontalLayout hlMarketScale = new HorizontalLayout();
        hlMarketScale.expand(marketSelect);
        hlMarketScale.add(marketSelect, scale);
        layout.add(name, hlBalanceSymbol, hlMarketScale);
        add(layout);

        symbol.addValueChangeListener(event -> balance.setSuffixComponent(new Span(symbol.getValue())));
        configureBinder();
    }

    @Override
    public SimpleAccount getEntity() {
        try {
            binder.writeBean(getSimpleAccount());
            //binder.writeBean(getSimpleAccount());
        } catch (ValidationException e) {
            e.printStackTrace();
        }
//        simpleAccount.setBalance(new AssetImpl(simpleAccount.getBalance()));
        return getSimpleAccount();
    }

    @Override
    public void setEntity(SimpleAccount entity) {
        this.simpleAccount = entity;
        binder.readBean(entity);
        if (simpleAccount.getId() != 0) {
            symbol.setEnabled(false);
            marketSelect.setEnabled(false);
            scale.setEnabled(false);
        }
        else {
            symbol.setEnabled(true);
            marketSelect.setEnabled(true);
            scale.setEnabled(true);
        }
    }

    @Override
    public void setDefaultValuesSource(SimpleAccount source) {
        if (source == null)
            defaultAccount = new SimpleAccount();
        else
            defaultAccount = source;
    }

    @Override
    public Class<SimpleAccount> getEntityClass() {
        return SimpleAccount.class;
    }

    @Override
    public void reset() {
        symbol.setEnabled(true);
        marketSelect.setEnabled(true);
        scale.setEnabled(true);
        setFieldDefaults();
        simpleAccount = creatNewSimpleAccountBasedOnDefaultValuesSource();
    }

    private void configureBinder() {
        binder.bind(name, SimpleAccount::getName, SimpleAccount::setName);
        binder.bind(balance, SimpleAccount::getBalance, SimpleAccount::setBalance);
        binder.bind(balance, acc -> acc.getBalance(), (acc, bal) -> {
            acc.setAssetType(new AssetType(symbol.getValue(), scale.getValue(), marketSelect.getValue()));
            acc.setBalance(bal);
        });
        binder.bind(marketSelect, acc -> acc.getAssetType().marketType(),
                (acc, market) -> acc.setAssetType(
                        new AssetType(acc.getAssetType().symbol(), acc.getAssetType().scale(), market))
        );
        binder.bind(symbol, acc -> acc.getAssetType().symbol(),
                (acc, sym) -> acc.setAssetType(
                        new AssetType(sym, acc.getAssetType().scale(), acc.getAssetType().marketType()))
        );
        binder.bind(scale, acc -> acc.getAssetType().scale(),
                (acc, sc) -> acc.setAssetType(
                        new AssetType(acc.getAssetType().symbol(), sc, acc.getAssetType().marketType()))
        );
    }

    private void setFieldDefaults() {
        name.clear();
        name.setPlaceholder("e.g. Cash");
        int defaultScale = 2;
        scale.clear();
        scale.setValue(defaultScale);
        balance.clear();
        balance.setValue(BigDecimal.ZERO.setScale(defaultScale));
        balance.setPlaceholder(balance.getValue().toPlainString());
        marketSelect.setValue(Market.Money);
        updateScaleFieldEnabled();
        symbol.clear();
        symbol.setPlaceholder("e.g. EUR");
        symbol.setValue("EUR");
    }

    private void configureMarketSelect() {
        marketSelect.setLabel("Market");
        marketSelect.setItems(Market.values());
        marketSelect.addValueChangeListener(event -> {
            updateScaleFieldEnabled();
        });
    }

    private void updateScaleFieldEnabled() {
        if (marketSelect.getValue() != Market.Money) scale.setEnabled(true);
        else scale.setEnabled(false);
    }

    private void configureBalanceField() {
        balance.setWidthFull();
        balance.setValueChangeMode(ValueChangeMode.EAGER);
        setBalanceScaleLimitation();
    }

    private void setBalanceScaleLimitation() {
        balance.addValueChangeListener(event -> {
            if (balance.getValue() != null && balance.getValue().scale() > scale.getValue()) {
                balance.setValue(balance.getValue().setScale(scale.getValue(), RoundingMode.DOWN));
            }
        });
    }

    private void configureScaleField() {
        scale.setWidth("30%");
        scale.setHasControls(true);
        scale.setMin(0);
        scale.setMax(19);
        scale.addValueChangeListener(event -> {
            if (scale.getValue() == null || scale.getValue() < 0) scale.setValue(0);
            if (scale.getValue() > 19) scale.setValue(19);
            updateBalanceValue();
        });
        scale.setEnabled(false);
    }

    private void updateBalanceValue() {
        if (balance.getValue() == null) {
            balance.setValue(BigDecimal.ZERO);
        }
        balance.setValue(balance.getValue().setScale(scale.getValue(), RoundingMode.DOWN));
    }

    private SimpleAccount getSimpleAccount() {
        if (simpleAccount == null) {
            simpleAccount = creatNewSimpleAccountBasedOnDefaultValuesSource();
        }
        return simpleAccount;
    }

    private SimpleAccount creatNewSimpleAccountBasedOnDefaultValuesSource() {
        return new SimpleAccount(defaultAccount.getName(), new Asset(defaultAccount.getBalance(), defaultAccount.getAssetType()));
    }
}
