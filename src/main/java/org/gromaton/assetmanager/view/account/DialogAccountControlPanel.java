package org.gromaton.assetmanager.view.account;

import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.view.GenericDialogControlPanel;
import org.gromaton.assetmanager.view.GenericInputFormForDialog;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope("prototype")
public class DialogAccountControlPanel extends GenericDialogControlPanel<Account> {
    public DialogAccountControlPanel(GenericInputFormForDialog<Account> inputFormForDialog) {
        super(inputFormForDialog);
    }
}
