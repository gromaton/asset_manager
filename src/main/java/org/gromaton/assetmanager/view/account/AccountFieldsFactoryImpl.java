package org.gromaton.assetmanager.view.account;

import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.NotSupportedEntityFields;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringComponent
@Scope("prototype")
public class AccountFieldsFactoryImpl implements AccountFieldsAbstractFactory {

    @Autowired
    List<GenericEntityFields<? extends Account>> implementationList;

    @Override
    public GenericEntityFields<? extends Account> create(Account account) {
        return implementationList.stream()
                .filter(e -> e.getEntityClass() == account.getClass())
                .findFirst()
                .orElseGet(() -> new NotSupportedEntityFields("This type is not supported"));
    }
}
