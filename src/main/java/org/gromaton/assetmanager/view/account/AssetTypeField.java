package org.gromaton.assetmanager.view.account;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import org.gromaton.assetmanager.domain.assets.AssetType;
import org.gromaton.assetmanager.domain.assets.Market;

class AssetTypeField extends CustomField<AssetType> {
    Select<Market> marketSelectField = new Select<>();
    TextField symbolField = new TextField("Symbol");
    IntegerField scaleField = new IntegerField("Scale");

    public AssetTypeField() {
        marketSelectField.addValueChangeListener(e -> updateValue());
        symbolField.addValueChangeListener(e -> updateValue());
        scaleField.addValueChangeListener(e -> updateValue());
        HorizontalLayout layoutAssetType = new HorizontalLayout();
        //layoutAssetType.setWidthFull();
        layoutAssetType.expand(marketSelectField);
        symbolField.setWidth("30%");
        layoutAssetType.add(marketSelectField, symbolField, scaleField);
        add(layoutAssetType);

    }

    Select<Market> getMarketSelectField() {
        return marketSelectField;
    }

    TextField getSymbolField() {
        return symbolField;
    }

    IntegerField getScaleField() {
        return scaleField;
    }

    @Override
    protected AssetType generateModelValue() {
        return new AssetType(symbolField.getValue(), scaleField.getValue(), marketSelectField.getValue());
    }

    @Override
    protected void setPresentationValue(AssetType assetType) {
        marketSelectField.setValue(assetType.marketType());
        symbolField.setValue(assetType.symbol());
        scaleField.setValue(assetType.scale());
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        symbolField.setEnabled(enabled);
        marketSelectField.setEnabled(enabled);
        scaleField.setEnabled(enabled);
    }
}