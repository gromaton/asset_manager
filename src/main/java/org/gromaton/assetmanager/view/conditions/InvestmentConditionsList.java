package org.gromaton.assetmanager.view.conditions;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;

public class InvestmentConditionsList extends MultiSelectListBox<AbstractConditions> {

    public InvestmentConditionsList() {
        this.setRenderer(new ComponentRenderer<>(e -> {
            Div content = new Div();
            Div type = new Div();
            type.setText(e.getTitle());
            Div description = new Div();
            description.getStyle().set("font-size", "0.8em");
            description.setText(e.toString());
            content.add(type, description);
            return content;
        }));
    }
}
