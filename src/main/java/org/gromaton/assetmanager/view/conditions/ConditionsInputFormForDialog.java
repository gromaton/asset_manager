package org.gromaton.assetmanager.view.conditions;

import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.gromaton.assetmanager.domain.conditions.ConditionsType;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.GenericInputFormForDialog;
import org.springframework.context.annotation.Scope;

import java.util.EnumSet;

@SpringComponent
@Scope("prototype")
public class ConditionsInputFormForDialog extends GenericInputFormForDialog<AbstractConditions> {
    private final Select<ConditionsType> typeSelect = new Select();
    private ConditionsFieldsAbstractFactory factory;

    public ConditionsInputFormForDialog(ConditionsFieldsAbstractFactory factory) {
        this.factory = factory;
        configureTypeSelect();
        addComponentAsFirst(typeSelect);
    }

    @Override
    protected GenericEntityFields<AbstractConditions> retrieveInputFields() {
        return (GenericEntityFields<AbstractConditions>) factory.create(typeSelect.getValue());
    }

    private void configureTypeSelect() {
        typeSelect.setLabel("Conditions Type");
        typeSelect.setItemLabelGenerator(ConditionsType::getName);
        typeSelect.setItems(EnumSet.allOf(ConditionsType.class));
        typeSelect.addValueChangeListener(event -> updateFields());
        typeSelect.setValue(ConditionsType.values()[0]);
        typeSelect.setWidthFull();
    }

    @Override
    public void read(AbstractConditions src) {
        typeSelect.setValue(ConditionsType.getForClass(src.getClass()));
        typeSelect.setEnabled(false);
        super.read(src);
    }

    @Override
    public void reset() {
        super.reset();
        typeSelect.setEnabled(true);
    }
}
