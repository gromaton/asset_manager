package org.gromaton.assetmanager.view.conditions;

import org.gromaton.assetmanager.domain.conditions.ConditionsType;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.view.GenericEntityFields;

public interface ConditionsFieldsAbstractFactory {
    GenericEntityFields<? extends AbstractConditions> create(ConditionsType conditionsType);
}
