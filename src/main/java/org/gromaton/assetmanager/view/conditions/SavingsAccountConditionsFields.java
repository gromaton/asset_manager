package org.gromaton.assetmanager.view.conditions;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.conditions.SavingsAccountConditions;
import org.gromaton.assetmanager.domain.recurrence.Monthly;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.recurrence.RecurrenceFields;
import org.gromaton.assetmanager.view.recurrence.RecurrenceFieldsFactory;
import org.springframework.context.annotation.Scope;

import java.math.BigDecimal;

@SpringComponent
@Scope("prototype")
public class SavingsAccountConditionsFields extends GenericEntityFields<SavingsAccountConditions> {
    private TextField title = new TextField("Title", "Conditions name");
    private NumberField interestRate = new NumberField("Annual Interest Rate, %", "0,0");
    private RecurrenceFields recurrenceFields;
    private Binder<SavingsAccountConditions> binder = new Binder<>(SavingsAccountConditions.class);
    private SavingsAccountConditions conditionsBean;
    private SavingsAccountConditions defaultConditions = new SavingsAccountConditions("default", 0.0, Monthly.on(1));


    public SavingsAccountConditionsFields(RecurrenceFieldsFactory recurrenceFieldsFactory) {
        VerticalLayout vLayout = new VerticalLayout(title);
        vLayout.setPadding(false);
        vLayout.setMargin(false);
        title.setWidthFull();
        interestRate.setWidthFull();
        vLayout.add(interestRate);

        Label payoutLabel = new Label("Payout settings");
        payoutLabel.getStyle().set("padding", "0px");
        payoutLabel.getStyle().set("margin-bottom", "0px");
        vLayout.add(payoutLabel);

        recurrenceFields = new RecurrenceFields(recurrenceFieldsFactory);
        vLayout.add(recurrenceFields);

        add(vLayout);

        binder.bind(title, "title");
        binder.bind(interestRate,
                bean -> BigDecimal.valueOf(bean.getInterestRate()).movePointRight(2).doubleValue(),
                (bean, value) -> bean.setInterestRate(BigDecimal.valueOf(value).movePointLeft(2).doubleValue())
        );
    }

    private SavingsAccountConditions createNewConditionsBasedOnDefaultValuesSource() {
        return new SavingsAccountConditions(defaultConditions);
    }

    @Override
    public SavingsAccountConditions getEntity() {
        conditionsBean = conditionsBean == null ?
                createNewConditionsBasedOnDefaultValuesSource() : conditionsBean;
        try {
            binder.writeBean(conditionsBean);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        conditionsBean.setPayout(recurrenceFields.getEntity());
        return conditionsBean;
    }

    @Override
    public void setEntity(SavingsAccountConditions entity) {
        if (entity == null) {
            conditionsBean = createNewConditionsBasedOnDefaultValuesSource();
        } else {
            this.conditionsBean = entity;
            binder.readBean(entity);
        }
        recurrenceFields.setEntity(entity.getPayout());
    }

    @Override
    public void setDefaultValuesSource(SavingsAccountConditions source) {
        if (source == null)
            defaultConditions = createNewConditionsBasedOnDefaultValuesSource();
        else
            defaultConditions = source;
    }

    @Override
    public Class<SavingsAccountConditions> getEntityClass() {
        return SavingsAccountConditions.class;
    }

    @Override
    public void reset() {
        title.clear();
        interestRate.setValue(0.0);
        conditionsBean = createNewConditionsBasedOnDefaultValuesSource();
        recurrenceFields.reset();
    }
}
