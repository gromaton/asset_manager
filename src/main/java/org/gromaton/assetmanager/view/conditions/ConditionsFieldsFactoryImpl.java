package org.gromaton.assetmanager.view.conditions;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.gromaton.assetmanager.domain.conditions.ConditionsType;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.NotSupportedEntityFields;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringComponent
@Scope("prototype")
public class ConditionsFieldsFactoryImpl implements ConditionsFieldsAbstractFactory {
    @Autowired
    List<GenericEntityFields<? extends AbstractConditions>> availableList;

    @Override
    public GenericEntityFields<? extends AbstractConditions> create(ConditionsType conditionsType) {
        return availableList.stream()
                .filter(e -> e.getEntityClass() == conditionsType.getConditionsClass())
                .findFirst()
                .orElseGet(() -> new NotSupportedEntityFields("Error: This type is not yet supported"));
    }
}
