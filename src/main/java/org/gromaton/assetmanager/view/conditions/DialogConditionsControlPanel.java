package org.gromaton.assetmanager.view.conditions;

import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.view.GenericDialogControlPanel;
import org.gromaton.assetmanager.view.GenericInputFormForDialog;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope("prototype")
public class DialogConditionsControlPanel extends GenericDialogControlPanel<AbstractConditions> {
    public DialogConditionsControlPanel(GenericInputFormForDialog<AbstractConditions> inputForm) {
        super(inputForm);
    }
}
