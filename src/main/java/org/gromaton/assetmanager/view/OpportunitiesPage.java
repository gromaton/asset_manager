package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.service.AccountService;
import org.gromaton.assetmanager.service.NewConditionsService;
import org.gromaton.assetmanager.view.conditions.InvestmentConditionsList;
import org.gromaton.assetmanager.view.predictions.AccountBasedPredictionRecord;
import org.gromaton.assetmanager.view.predictions.PredictionRecord;
import org.gromaton.assetmanager.view.predictions.PredictionTable;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Route(value = "/opportunities", layout = MainLayout.class)
@PageTitle("Opportunities | AMS")
public class OpportunitiesPage extends VerticalLayout {
    NewConditionsService conditionsService;
    AccountService accountService;

    @Autowired
    GenericDialogControlPanel<AbstractConditions> conditionsControlPanel;
    InvestmentConditionsList conditionsList = new InvestmentConditionsList();
    ListBox<Account> accountList = new ListBox<>();
    DatePicker investStartDate = new DatePicker("From", LocalDate.now());
    DatePicker investEndDate = new DatePicker("To", LocalDate.now().plusYears(1));
    Button calcButton = new Button("Calculate");

    @PostConstruct
    private void init() {
        add(new H3("Investment Opportunities"));
//        addConditionsBlock();
//        addAccountsBlock();
        addMainBlock();
        addInvestmentsIntervalForm();
        addCalcResultsBlock();
    }

    public OpportunitiesPage(NewConditionsService conditionsService, AccountService accountService) {
        this.conditionsService = conditionsService;
        this.accountService = accountService;
    }

    private void addMainBlock() {
        HorizontalLayout hLayout = new HorizontalLayout(createConditionsBlock(), createAccountsBlock());
        hLayout.setMargin(true);
        add(hLayout);
    }

    private Div createConditionsBlock() {
        Div conditionsDiv = new Div();
        conditionsDiv.getStyle().set("margin", "0em 2em 0em 0em");
        Label conditionsCaption = new Label("Available offers to compare");
        conditionsCaption.getStyle().set("font-weight", "bold");
        conditionsDiv.add(
                conditionsCaption,
                conditionsControlPanel,
                conditionsList
        );
        refreshConditionsListContent();
        configureConditionsControlPanelListeners();
        configureConditionsControlPanelButtonsDisabling();
        configureConditionsListListeners();
        return conditionsDiv;
    }

    private Div createAccountsBlock() {
        Div accountsDiv = new Div();
        accountsDiv.getStyle().set("margin", "0em 0em 0em 0em");
        Label accountsCaption = new Label("Use with the following account");
        accountsCaption.getStyle().set("font-weight", "bold");
        accountsDiv.add(
                accountsCaption,
                accountList
        );
        accountList.setItems(accountService.findAll());
        configureAccountListListeners();
        return accountsDiv;
    }

    private void refreshConditionsListContent() {
        conditionsList.setItems(conditionsService.findAll());
    }

    private void configureConditionsControlPanelListeners() {
        conditionsControlPanel.addRefreshClickListener(event -> refreshConditionsListContent());
        conditionsControlPanel.addSaveListener(event -> {
            conditionsService.save(event.getEntity());
            refreshConditionsListContent();
        });
        conditionsControlPanel.addDeleteClickListener(event -> {
            conditionsService.deleteAll(conditionsList.getSelectedItems());
            refreshConditionsListContent();
        });
        conditionsControlPanel.addEditClickListener(event -> {
            conditionsControlPanel.read(conditionsList.getSelectedItems().iterator().next());
            conditionsControlPanel.openDialog();
        });
        conditionsControlPanel.addCreateClickListener(event -> {
            conditionsControlPanel.reset();
            conditionsControlPanel.openDialog();
        });
    }

    private void configureConditionsControlPanelButtonsDisabling() {
        conditionsControlPanel.getMenuBar().getEditItem().setEnabled(conditionsList.getSelectedItems() != null && conditionsList.getSelectedItems().size() == 1);
        conditionsControlPanel.getMenuBar().getDeleteItem().setEnabled(conditionsList.getSelectedItems() != null && !conditionsList.getSelectedItems().isEmpty());
    }

    private void configureConditionsListListeners() {
        conditionsList.addSelectionListener(event -> {
            configureConditionsControlPanelButtonsDisabling();
            updateCalcButtonDisabling();
        });
    }

    private void configureAccountListListeners() {
        accountList.addValueChangeListener(event -> {
           updateCalcButtonDisabling();
        });
    }

    private void addInvestmentsIntervalForm() {
        Div intervalDiv = new Div();
        Label intervalCaption = new Label("Choose investment interval");
        intervalCaption.getStyle().set("font-weight", "bold");
        intervalDiv.add(intervalCaption);
        /*investStartDate.getStyle().set("margin-right", "1em");
        Div intervalForm = new Div(investStartDate, investEndDate);*/
        investStartDate.setLocale(Locale.GERMANY);
        investStartDate.setWidth("9em");
        investStartDate.setEnabled(false);
        investEndDate.setLocale(Locale.GERMANY);
        investEndDate.setWidth("9em");
//        Button plusMonth = new Button("+1 Month");
//        plusMonth.addClickListener(click -> investEndDate.setValue(investEndDate.getValue().plusMonths(1)));
        Button plusYearButton = new Button("+1Y");
        plusYearButton.setWidth("3em");
        plusYearButton.addClickListener(click -> investEndDate.setValue(investEndDate.getValue().plusYears(1)));
        configureCalcButton();
        HorizontalLayout intervalForm = new HorizontalLayout();
        intervalForm.setDefaultVerticalComponentAlignment(Alignment.END);
        intervalForm.add(investStartDate, investEndDate, plusYearButton, calcButton);
        intervalDiv.add(intervalForm);
        add(intervalDiv);
    }

    private void configureCalcButton() {
        calcButton.setEnabled(false);
        calcButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    private void updateCalcButtonDisabling() {
        calcButton.setEnabled(conditionsList.getSelectedItems() != null &&
                !conditionsList.getSelectedItems().isEmpty() &&
                accountList.getValue() != null);
    }

    private List<PredictionRecord> generateResultRecords() {
        Account account = accountList.getValue();
        Set<AbstractConditions> conditionsSet = conditionsList.getSelectedItems();
        List<PredictionRecord> result = new ArrayList<>(conditionsSet.size());
        if (conditionsSet != null && !conditionsSet.isEmpty() && account != null) {
            for (AbstractConditions conditions : conditionsSet) {
                result.add(new AccountBasedPredictionRecord(account, conditions, LocalDateTime.of(investEndDate.getValue(), LocalTime.MAX)));
            }
        }
        return result;
    }

    private void addCalcResultsBlock() {
        PredictionTable predictionTable = new PredictionTable();
        predictionTable.setColumnsAutoWidth(true);
        predictionTable.setHeightByRows(true);
        Div calcDiv = new Div();
        Label calculationsCaption = new Label("Estimated results");
        calculationsCaption.getStyle().set("font-weight", "bold");
        calcDiv.add(calculationsCaption);
        calcDiv.add(predictionTable);
        calcDiv.setVisible(false);
        add(calcDiv);

        calcButton.addClickListener(click -> {
            predictionTable.setItems(generateResultRecords());
            calcDiv.setVisible(true);
        });

    }
}
