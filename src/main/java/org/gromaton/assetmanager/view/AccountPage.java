package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Route(value = "/accounts", layout = MainLayout.class)
@PageTitle("Accounts | AMS")
public class AccountPage extends VerticalLayout {
    Grid<Account> accountsGrid = new Grid<>(Account.class);
    private Set<Account> selectedAccounts = new HashSet<>();

    private AccountService accountService;
    @Autowired
    private GenericDialogControlPanel<Account> accountControlPanel;

    @PostConstruct
    public void init() {
        configureAccountControlPanelListeners();
        configureAccountControlPanelButtonsDisabling();
        addComponentAtIndex(indexOf(accountsGrid), accountControlPanel);
    }

    public AccountPage(AccountService accountService) {
        this.accountService = accountService;
        configureAccountGrid();
        configureAccountGridListeners();
        add(new H3("Account list"));
        add(accountsGrid);
        refreshAccountGridContent();
    }

    private void configureAccountControlPanelListeners() {
        accountControlPanel.addRefreshClickListener(event -> refreshAccountGridContent());
        //ToDo: Add delete confirmation dialog
        accountControlPanel.addDeleteClickListener(event -> {
            accountService.deleteAll(accountsGrid.getSelectedItems());
            refreshAccountGridContent();
        });
        accountControlPanel.addSaveListener(event -> {
            accountService.save(event.getEntity());
            refreshAccountGridContent();
        });
        accountControlPanel.addEditClickListener(event -> {
            accountControlPanel.read(selectedAccounts.iterator().next());
            accountControlPanel.openDialog();
        });
        accountControlPanel.addCreateClickListener(event -> {
            accountControlPanel.reset();
            accountControlPanel.openDialog();
        });
    }

    private void configureAccountGridListeners() {
        accountsGrid.addSelectionListener(event -> {
            selectedAccounts = accountsGrid.getSelectedItems();
            configureAccountControlPanelButtonsDisabling();
        });
    }

    //ToDo: think about how to get rid of getMenuMar() method here. (by adding setSelectedItems method or by passing selectable component into control panel, or on event)
    private void configureAccountControlPanelButtonsDisabling() {
        accountControlPanel.getMenuBar().getEditItem().setEnabled(selectedAccounts != null && selectedAccounts.size() == 1);
        accountControlPanel.getMenuBar().getDeleteItem().setEnabled(selectedAccounts != null && !selectedAccounts.isEmpty());
    }

    private void configureAccountGrid() {
        accountsGrid.setHeightByRows(true);
        accountsGrid.removeAllColumns();
        accountsGrid.addColumn(Account::getName).setHeader("Name");
        accountsGrid.addColumn(e -> e.getBalance().toPlainString()).setHeader("Balance");
        accountsGrid.addColumn(Account::getSymbol).setHeader("Symbol");
        accountsGrid.addColumn(Account::typeTitle).setHeader("Type");
        accountsGrid.addThemeVariants(GridVariant.LUMO_NO_ROW_BORDERS, GridVariant.LUMO_ROW_STRIPES);
        accountsGrid.setSelectionMode(Grid.SelectionMode.MULTI);
    }

    private void refreshAccountGridContent() {
        accountsGrid.setItems(accountService.findAll());
    }
}
