package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import org.gromaton.assetmanager.domain.IEntity;

@Tag("div")
public abstract class GenericEntityFields<T extends IEntity> extends Component implements HasComponents {
    public abstract T getEntity();
    public abstract void setEntity(T entity);
    public abstract void setDefaultValuesSource(T source);
    public abstract Class<T> getEntityClass();
    public abstract void reset();
}