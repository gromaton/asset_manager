package org.gromaton.assetmanager.view.predictions;

import com.vaadin.flow.component.grid.Grid;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class PredictionTable extends PredictionView {
    private Grid<PredictionRecord> grid = new Grid<>();
    private boolean allColumnsAutoWidth;

    public PredictionTable() {
        add(grid);
    }

    @Override
    public boolean setItems(Collection<? extends PredictionRecord> items) {
        grid.removeAllColumns();
        Optional<? extends PredictionRecord> someRecord = items.stream().findAny();
        if (!someRecord.isPresent()) {
            return false;
        }
        grid.setItems(items.stream().collect(Collectors.toList()));
        for (String paramName : someRecord.get().getAllParamNames()) {
            grid.addColumn(e -> e.getValue(paramName)).setHeader(paramName).setKey(paramName);
        }
        setColumnsAutoWidth(allColumnsAutoWidth);
        return true;
    }

    public void setColumnsAutoWidth(boolean autoWidth) {
        this.allColumnsAutoWidth = autoWidth;
        for (Grid.Column column : grid.getColumns()) {
            column.setAutoWidth(autoWidth);
        }
    }

    public void setHeightByRows(boolean heightByRows) {
        grid.setHeightByRows(heightByRows);
    }

}
