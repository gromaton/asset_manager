package org.gromaton.assetmanager.view.predictions;

import org.gromaton.assetmanager.domain.accounts.Account;
import org.gromaton.assetmanager.domain.conditions.AbstractConditions;
import org.gromaton.assetmanager.domain.conditions.BalanceBehavior;
import org.gromaton.assetmanager.domain.conditions.BalanceBehaviorFactory;
import org.gromaton.assetmanager.domain.conditions.BalanceBehaviorFactoryImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

public class AccountBasedPredictionRecord implements PredictionRecord {
    private final Account account;
    private final AbstractConditions conditions;
    private final LocalDateTime targetTimeStamp;
    private BalanceBehavior balanceBehavior;
    private Map<String, String> record;

    public AccountBasedPredictionRecord(Account account, AbstractConditions conditions, LocalDateTime targetTimeStamp) {
        this.account = Optional.of(account).get();
        this.conditions = Optional.of(conditions).get();
        this.targetTimeStamp = Optional.of(targetTimeStamp).get();
        BalanceBehaviorFactory bbFactory = new BalanceBehaviorFactoryImpl();
        balanceBehavior = bbFactory.createFor(account);
        initRecord();
    }

    @Override
    public List<String> getAllParamNames() {
        return new ArrayList<>(record.keySet());
    }

    @Override
    public String getValue(String paramName) {
        return record.get(paramName);
    }

    private void initRecord() {
        BigDecimal initialFunds = calcInitialFunds();
        BigDecimal topups = calcTopups();
        BigDecimal balanceOnTargetDay = calcBalanceOnTargetDay();
        record = new LinkedHashMap<>();
        record.put("Offer", conditions.getTitle());
        record.put("Initial Funds", initialFunds + " " + account.getSymbol());
        record.put("Additional Top-ups", topups + " " + account.getSymbol());
        record.put("Profit", balanceOnTargetDay.subtract(topups).subtract(initialFunds) + " " + account.getSymbol());
        record.put("Target Day", balanceOnTargetDay + " " + account.getSymbol());
    }

    private BigDecimal calcInitialFunds() {
        return account.getBalance();
    }

    private BigDecimal calcTopups() {
        return balanceBehavior.balanceOn(targetTimeStamp).subtract(balanceBehavior.balanceOn(LocalDateTime.now()));
    }

    private BigDecimal calcBalanceOnTargetDay() {
        return conditions.calcBalance(balanceBehavior, LocalDateTime.now(), targetTimeStamp);
    }
}
