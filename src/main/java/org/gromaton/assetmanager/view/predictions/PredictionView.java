package org.gromaton.assetmanager.view.predictions;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;

import java.util.Collection;

@Tag("div")
public abstract class PredictionView extends Component implements HasComponents {
    abstract boolean setItems(Collection<? extends PredictionRecord> items);
}
