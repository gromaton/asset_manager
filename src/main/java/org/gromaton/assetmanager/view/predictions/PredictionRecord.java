package org.gromaton.assetmanager.view.predictions;

import java.util.List;

public interface PredictionRecord {
    List<String> getAllParamNames();
    String getValue(String paramName);

}
