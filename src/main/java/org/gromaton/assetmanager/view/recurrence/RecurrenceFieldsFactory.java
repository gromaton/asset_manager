package org.gromaton.assetmanager.view.recurrence;

import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;

import java.util.List;

public interface RecurrenceFieldsFactory {
    GenericEntityFields<RecurrenceCondition> create(Class<? extends RecurrenceCondition> recurrenceClass);
    List<String> getAllTitles();
    List<Class<? extends RecurrenceCondition>> getAllClasses();
    Class<? extends RecurrenceCondition> getClass(String title);
    String getTitle(Class<? extends RecurrenceCondition> recurrenceClass);
}
