package org.gromaton.assetmanager.view.recurrence;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.select.Select;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;

public class RecurrenceFields extends GenericEntityFields<RecurrenceCondition> implements HasComponents {
    private GenericEntityFields<RecurrenceCondition> fields;
    private RecurrenceFieldsFactory fieldFactory;

    Select<Class<? extends RecurrenceCondition>> typeSelect = new Select<>();

    //ToDo: Don't like the design: can't make this class as Component because of cycle dependency in the fieldFactory,
    // maybe cagrfd sdf sdf

    public RecurrenceFields(RecurrenceFieldsFactory fieldFactory) {
        this.fieldFactory = fieldFactory;
        configureTypeSelect(fieldFactory);
        add(typeSelect);
    }

    @Override
    public RecurrenceCondition getEntity() {
        return fields.getEntity();
    }

    @Override
    public void setEntity(RecurrenceCondition entity) {
        reconfigureFieldsForUsingWith(entity.getClass());
        fields.setEntity(entity);
    }

    @Override
    public void setDefaultValuesSource(RecurrenceCondition source) {
        reconfigureFieldsForUsingWith(source.getClass());
        fields.setDefaultValuesSource(source);
    }

    @Override
    public Class<RecurrenceCondition> getEntityClass() {
        return fields.getEntityClass();
    }

    @Override
    public void reset() {
        fields.reset();
//        configureTypeSelect(fieldFactory);
    }

    private void configureTypeSelect(RecurrenceFieldsFactory fieldFactory) {
        typeSelect.setLabel("Recurrence type");
        typeSelect.setItems(fieldFactory.getAllClasses());
        typeSelect.setItemLabelGenerator(item -> fieldFactory.getTitle(item));
        typeSelect.addValueChangeListener(event -> {
            if (fields != null) {
                remove(fields);
            }
            fields = fieldFactory.create(typeSelect.getValue());
            add(fields);
        });
        typeSelect.setValue(fieldFactory.getAllClasses().get(0));
    }

    private void reconfigureFieldsForUsingWith(Class<? extends RecurrenceCondition> conditionClass) {
        //recurrenceFields are adjusted in ValueChangeListener of typeSelect after setValue is invoked
        typeSelect.setValue(conditionClass);
    }
}
