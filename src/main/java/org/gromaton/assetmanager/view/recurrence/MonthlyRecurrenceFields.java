package org.gromaton.assetmanager.view.recurrence;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.recurrence.Monthly;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.springframework.context.annotation.Scope;

@SpringComponent("monthly")
@Scope("prototype")
public class MonthlyRecurrenceFields extends GenericEntityFields<Monthly> {
    Select<Integer> daySelect = new Select<>();
    //Label textLabel = new Label();

    private Monthly defaultValues = Monthly.on(1);
    private Monthly formBean;

    public MonthlyRecurrenceFields() {
        this.getElement().getStyle().set("margin", "0px");
        this.getElement().getStyle().set("padding", "0px");

        configureDaySelect();
        VerticalLayout vLayout = new VerticalLayout(daySelect/*, textLabel*/);
        vLayout.setPadding(false);
        vLayout.setMargin(false);
        vLayout.setSpacing(false);
        setFieldDefaults();
        add(vLayout);
    }

    @Override
    public Monthly getEntity() {
        //Monthly result = getFormBean();
        return Monthly.on(daySelect.getValue());
    }

    @Override
    public void setEntity(Monthly entity) {
        formBean = entity != null ? entity : createNewMonthlyBasedOnDefaultValuesSource();
        daySelect.setValue(formBean.getDayOfMonth());
    }

    @Override
    public void setDefaultValuesSource(Monthly source) {
        if (source == null)
            defaultValues = Monthly.on(1);
        else
            this.defaultValues = source;
    }

    @Override
    public Class<Monthly> getEntityClass() {
        return Monthly.class;
    }

    @Override
    public void reset() {
        setFieldDefaults();
        formBean = createNewMonthlyBasedOnDefaultValuesSource();
    }

    private void configureDaySelect() {
        daySelect.getStyle().set("margin", "0px");
        daySelect.getStyle().set("padding", "0px");
        daySelect.setWidthFull();
        daySelect.setLabel("Recur every month on 1st");
        daySelect.setItems(createDayList());
        daySelect.addValueChangeListener(event ->
                        daySelect.setLabel(generateDayDependantLabelText(daySelect.getValue()))
        );
    }
    private Integer[] createDayList() {
        Integer[] result = new Integer[31];
        for (int i = 1; i <= 31; i++) {
            result[i-1] = +i;
        }
        return result;
    }

    private String generateDayDependantLabelText(int day) {
        if (day == 31) return "Recur every last day of month";
        else if (day > 27) return "Recur monthly on " + day + "th or last day";
        else if (day == 1) return "Recur every month on 1st";
        else if (day == 2) return "Recur every month on 2nd";
        else if (day == 3) return "Recur every month on 3rd";
        else return "Recur every month on " + day + "th";
    }

    private void setFieldDefaults() {
        daySelect.setValue(defaultValues.getDayOfMonth());
    }

    private Monthly getFormBean() {
        if (formBean == null) {
            formBean = createNewMonthlyBasedOnDefaultValuesSource();
        }
        return formBean;
    }

    private Monthly createNewMonthlyBasedOnDefaultValuesSource() {
        return Monthly.on(defaultValues.getDayOfMonth());
    }

}
