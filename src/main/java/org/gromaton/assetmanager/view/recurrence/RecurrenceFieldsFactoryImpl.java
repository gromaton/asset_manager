package org.gromaton.assetmanager.view.recurrence;

import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.gromaton.assetmanager.view.NotSupportedEntityFields;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class RecurrenceFieldsFactoryImpl implements RecurrenceFieldsFactory {
    private List<GenericEntityFields<RecurrenceCondition>> availableRecurrenceFields;

    public RecurrenceFieldsFactoryImpl(List<GenericEntityFields<? extends RecurrenceCondition>> availableRecurrenceFields) {
        this.availableRecurrenceFields = availableRecurrenceFields
                .stream()
                .map(e -> (GenericEntityFields<RecurrenceCondition>) e)
                .collect(Collectors.toList());
    }

    @Override
    public GenericEntityFields<RecurrenceCondition> create(Class<? extends RecurrenceCondition> recurrenceClass) {
            return availableRecurrenceFields.stream()
                .filter(e -> e.getEntityClass().equals(recurrenceClass))
                .findFirst()
                .orElseGet(() -> new NotSupportedEntityFields("Error: This recurrence type is not supported"));
    }

    @Override
    public List<String> getAllTitles() {
        return availableRecurrenceFields.stream().map(e -> e.getEntity().title()).collect(Collectors.toList());
    }

    @Override
    public List<Class<? extends RecurrenceCondition>> getAllClasses() {
        return availableRecurrenceFields.stream().map(e -> e.getEntityClass()).collect(Collectors.toList());
    }

    @Override
    public Class<? extends RecurrenceCondition> getClass(String title) {
        //return availableRecurrenceFields.stream().filter(e -> e.getEntity().title().equals(title)).findFirst().;
        return null;
    }

    @Override
    public String getTitle(Class<? extends RecurrenceCondition> recurrenceClass) {
        try {
            return recurrenceClass.newInstance().title();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
