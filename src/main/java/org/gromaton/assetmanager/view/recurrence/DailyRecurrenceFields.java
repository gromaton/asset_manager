package org.gromaton.assetmanager.view.recurrence;

import com.vaadin.flow.spring.annotation.SpringComponent;
import org.gromaton.assetmanager.domain.recurrence.Daily;
import org.gromaton.assetmanager.domain.recurrence.RecurrenceCondition;
import org.gromaton.assetmanager.view.GenericEntityFields;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope("prototype")
public class DailyRecurrenceFields extends GenericEntityFields<Daily> {
    public DailyRecurrenceFields() {
    }

    @Override
    public Daily getEntity() {
        return new Daily();
    }

    @Override
    public void setEntity(Daily entity) {
        //no action needed as Daily has no any stored fields
    }

    @Override
    public void setDefaultValuesSource(Daily source) {
        //no action needed as Daily has no any stored fields
    }

    @Override
    public Class<Daily> getEntityClass() {
        return Daily.class;
    }

    @Override
    public void reset() {
        //no action needed as Daily has no any stored fields
    }
}
