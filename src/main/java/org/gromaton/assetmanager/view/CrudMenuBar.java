package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;

public class CrudMenuBar extends MenuBar {
    MenuItem createItem;
    MenuItem refreshItem;
    MenuItem editItem;
    MenuItem deleteItem;

    public CrudMenuBar() {
        createItem = addItem(new Icon(VaadinIcon.PLUS_CIRCLE));
        refreshItem = addItem(new Icon(VaadinIcon.REFRESH));
        editItem = addItem(new Icon(VaadinIcon.EDIT));
        deleteItem = addItem(new Icon(VaadinIcon.TRASH));
    }

    public MenuItem getCreateItem() {
        return createItem;
    }

    public MenuItem getRefreshItem() {
        return refreshItem;
    }

    public MenuItem getEditItem() {
        return editItem;
    }

    public MenuItem getDeleteItem() {
        return deleteItem;
    }
}
