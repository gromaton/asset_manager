package org.gromaton.assetmanager.view;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.shared.Registration;
import org.gromaton.assetmanager.domain.IEntity;

@Tag("div")
public abstract class GenericInputForm<T extends IEntity> extends Component implements HasComponents {
    private GenericEntityFields<T> formFields;
    private Div fieldsDiv = new Div();

    protected abstract GenericEntityFields<T> retrieveInputFields();

    public GenericInputForm() {
        add(fieldsDiv);
    }

    public void updateFields() {
        fieldsDiv.removeAll();
        formFields = retrieveInputFields();
        fieldsDiv.add(formFields);
    }

    public void read(T src) {
        formFields.setEntity(src);
    }

    public void setDefaultValuesSource(T entity) {
        formFields.setDefaultValuesSource(entity);
    }

    public void reset() {
        formFields.reset();
    }

    private Class getCloseEventClass() {
        return CloseEvent.class;
    }

    private Class getSaveEventClass() {
        return SaveEvent.class;
    }

    public Registration addCloseListener(ComponentEventListener<CloseEvent> listener) {
        return addListener(getCloseEventClass(), listener);
    }

    public Registration  addSaveListener(ComponentEventListener<SaveEvent> listener) {
        return super.addListener(getSaveEventClass(), listener);
    }

    private abstract class InputFormEvent extends ComponentEvent<GenericInputForm<T>> {
        T persistedEntity;

        public InputFormEvent(GenericInputForm source, T persistedEntity) {
            super(source, false);
            this.persistedEntity = persistedEntity;
        }

        public T getEntity() {
            return this.persistedEntity;
        }
    }

    public class SaveEvent extends InputFormEvent {
        public SaveEvent(GenericInputForm source) {
            super(source, formFields.getEntity());
        }
    }

    public class CloseEvent extends InputFormEvent {
        public CloseEvent(GenericInputForm source) {
            super(source, null);
        }
    }
}
