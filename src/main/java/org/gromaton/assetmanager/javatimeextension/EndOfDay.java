package org.gromaton.assetmanager.javatimeextension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;

public class EndOfDay implements TemporalAdjuster {

    @Override
    public Temporal adjustInto(Temporal temporal) {
        return LocalDateTime.of(
                LocalDate.of(
                        temporal.get(ChronoField.YEAR),
                        temporal.get(ChronoField.MONTH_OF_YEAR),
                        temporal.get(ChronoField.DAY_OF_MONTH)
                ),
                LocalTime.MAX
        );
    }
}
